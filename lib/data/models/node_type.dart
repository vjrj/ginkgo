enum NodeType {
  duniter,
  cesiumPlus,
  gva,
  endpoint,
  duniterIndexer,
  datapodEndpoint,
  ipfsGateway
}
