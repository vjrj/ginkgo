// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'env.dart';

// **************************************************************************
// EnviedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
final class _Env {
  static const String currency = 'g1';

  static const List<int> _enviedkeysentryDsn = <int>[
    3826182180,
    3448174722,
    2375811811,
    1556105486,
    3649627460,
    2569872558,
    3179512492,
    1473258011,
    1792161183,
    1813247683,
    1671469938,
    2382346270,
    583217726,
    259915993,
    2348588801,
    31469926,
    748485219,
    1498358387,
    1757676553,
    300670654,
    2044220030,
    398092979,
    2892502955,
    2340727297,
    3261443925,
    4024944097,
    3047157593,
    1658471148,
    450612405,
    3642937030,
    2571677048,
    1679699856,
    2894164369,
    2775999580,
    4286473772,
    3222904016,
    1235616023,
    3040067773,
    2648844337,
    706717497,
    881721290,
    3597683196,
    1671370095,
    2634541862,
    2030998593,
    1306532544,
    3746997137,
    113708384,
    212127156,
    3494362847,
    2187434371,
    274263683,
    3223494104,
    3657696887,
    2825789688,
    726960873,
    1828748106,
    472395482,
    2897777648,
    4119607505,
    173190659,
  ];

  static const List<int> _envieddatasentryDsn = <int>[
    3826182220,
    3448174838,
    2375811735,
    1556105598,
    3649627447,
    2569872532,
    3179512451,
    1473258036,
    1792161196,
    1813247731,
    1671469892,
    2382346285,
    583217674,
    259916012,
    2348588898,
    31469828,
    748485211,
    1498358340,
    1757676652,
    300670683,
    2044219978,
    398093014,
    2892502938,
    2340727394,
    3261443895,
    4024944003,
    3047157564,
    1658471125,
    450612357,
    3642937076,
    2571677003,
    1679699958,
    2894164467,
    2775999592,
    4286473805,
    3222903990,
    1235616116,
    3040067720,
    2648844375,
    706717530,
    881721226,
    3597683087,
    1671369994,
    2634541896,
    2030998581,
    1306532530,
    3746997224,
    113708366,
    212127191,
    3494362800,
    2187434478,
    274263798,
    3223494070,
    3657696786,
    2825789579,
    726960839,
    1828748069,
    472395432,
    2897777559,
    4119607550,
    173190709,
  ];

  static final String? sentryDsn = String.fromCharCodes(List<int>.generate(
    _envieddatasentryDsn.length,
    (int i) => i,
    growable: false,
  ).map((int i) => _envieddatasentryDsn[i] ^ _enviedkeysentryDsn[i]));

  static const List<int> _enviedkeygitLabToken = <int>[
    2006290859,
    4281095164,
    3777758143,
    3497419970,
    3518986845,
    1103059359,
    879406990,
    1101534343,
    769793526,
    2188784112,
    3991047156,
    2090451402,
    1064360920,
    3663789202,
    4216407525,
    390169403,
    3364586679,
    231983091,
    2644215881,
    3791438002,
  ];

  static const List<int> _envieddatagitLabToken = <int>[
    2006290899,
    4281095062,
    3777758151,
    3497419930,
    3518986761,
    1103059433,
    879407037,
    1101534429,
    769793444,
    2188784010,
    3991047103,
    2090451385,
    1064360891,
    3663789222,
    4216407478,
    390169451,
    3364586723,
    231983037,
    2644215933,
    3791438017,
  ];

  static final String gitLabToken = String.fromCharCodes(List<int>.generate(
    _envieddatagitLabToken.length,
    (int i) => i,
    growable: false,
  ).map((int i) => _envieddatagitLabToken[i] ^ _enviedkeygitLabToken[i]));

  static const String duniterNodes =
      'https://g1.duniter.org https://g1.le-sou.org https://g1.cgeek.fr https://g1.monnaielibreoccitanie.org https://g1.duniter.fr https://g1.le-sou.org https://g1.cgeek.fr';

  static const String cesiumPlusNodes =
      'https://g1.data.le-sou.org https://g1.data.e-is.pro https://g1.data.presles.fr https://g1.data.mithril.re https://g1.data.brussels.ovh https://g1.data.pini.fr';

  static const String gvaNodes =
      'https://g1v1.p2p.legal/gva https://g1.asycn.io/gva https://duniter.pini.fr/gva https://duniter-v1.comunes.net/gva';

  static const String endPoints =
      'wss://gdev.cgeek.fr/ws wss://gdev.trentesaux.fr/ws wss://gdev.gyroi.de/ws wss://gdev-smith.pini.fr/ws wss://gdev.pini.fr/ws wss://gdev.txmn.tk/ws wss://duniter-v2-vjrj-gdev.comunes.net/ws wss://bulmagdev.sleoconnect.fr/ws wss://gdev.cuates.net/ws wss://gdev.matograine.fr/ws wss://gdev.p2p.legal/ws';

  static const String duniterIndexerNodes =
      'https://squid.gdev.coinduf.eu/v1/graphql https://squid.gdev.gyroi.de/v1/graphql https://duniter-v2-vjrj-squid.comunes.net/v1/graphql';

  static const String datapodEndpoints =
      'https://datapod.gyroi.de/v1/graphql https://datapod.coinduf.eu/v1/graphql';

  static const String ipfsGateways =
      'https://gyroi.de https://gateway.datapod.coinduf.eu/';
}
