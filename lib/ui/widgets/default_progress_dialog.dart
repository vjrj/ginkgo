import 'package:flutter/material.dart';
import 'package:sn_progress_dialog/enums/progress_types.dart';

ProgressType defProgressType = ProgressType.determinate;
bool defProgressHideValue = true;
Color defProgressBgColor = Colors.white70;
bool defProgressBarrierDismissible = true;
int defProgressMsgMaxLines = 3;
