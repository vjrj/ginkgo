import 'package:flutter/material.dart';

// first screen
final GlobalKey creditCardKey = GlobalKey(debugLabel: 'creditCardPubKey');
final GlobalKey creditCardPubKey = GlobalKey(debugLabel: 'creditCardPubKey');
final GlobalKey paySearchUserKey = GlobalKey(debugLabel: 'paySearchUserKey');
final GlobalKey payAmountKey = GlobalKey(debugLabel: 'payAmountKey');
final GlobalKey paySentKey = GlobalKey(debugLabel: 'paySentKey');

// second screen
final GlobalKey receiveMainKey = GlobalKey(debugLabel: 'receiveMainKey');
final GlobalKey receiveQrKey = GlobalKey(debugLabel: 'receiveQrKey');
final GlobalKey receiveAmountKey = GlobalKey(debugLabel: 'receiveAmountKey');
final GlobalKey receiveSumKey = GlobalKey(debugLabel: 'receiveSumKey');

// third screen
final GlobalKey contactsMainKey = GlobalKey(debugLabel: 'contactsMainKey');
final GlobalKey contactsQrKey = GlobalKey(debugLabel: 'contactsQrKey');

// fourth screen
final GlobalKey txMainKey = GlobalKey(debugLabel: 'txMainKey');
final GlobalKey txRefreshKey = GlobalKey(debugLabel: 'txRefreshKey');
final GlobalKey txBalanceKey = GlobalKey(debugLabel: 'txBalanceKey');

// fifth screen
final GlobalKey infoMainKey = GlobalKey(debugLabel: 'infoMainKey');
final GlobalKey exportMainKey = GlobalKey(debugLabel: 'exportMainKey');
