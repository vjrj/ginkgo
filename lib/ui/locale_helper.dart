import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

String currentLocale(BuildContext context) => context.locale.languageCode;
