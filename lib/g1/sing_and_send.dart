import 'dart:async';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:polkadart/polkadart.dart';
import 'package:polkadart/scale_codec.dart';
import 'package:polkadart_keyring/polkadart_keyring.dart';

import '../data/models/node.dart';
import '../generated/gdev/gdev.dart';
import '../generated/gdev/types/gdev_runtime/runtime_call.dart';
import '../generated/gdev/types/primitive_types/h256.dart';
import '../ui/logger.dart';
import 'duniter_endpoint_helper.dart';

class SignAndSendResult {
  SignAndSendResult({
    this.node,
    required this.progressStream,
  });

  final Node? node;
  final Stream<String> progressStream;
}

Future<SignAndSendResult> signAndSend(Node node, Provider provider,
    Gdev polkadot, KeyPair wallet, RuntimeCall call,
    {required String Function(String statusType) messageTransformer,
    Duration timeout = defPolkadotTimeout}) async {
  final StreamController<String> progressController =
      StreamController<String>();

  try {
    progressController.add(messageTransformer('building_transaction'));

    final RuntimeVersion runtimeVersion =
        await polkadot.rpc.state.getRuntimeVersion();
    final int currentBlockNumber = (await polkadot.query.system.number()) - 1;
    final H256 currentBlockHash =
        await polkadot.query.system.blockHash(currentBlockNumber);
    final int nonce =
        await polkadot.rpc.system.accountNextIndex(wallet.address);
    final H256 genesisHash = await polkadot.query.system.blockHash(0);

    final Uint8List encodedCall = call.encode();

    final Uint8List payload = SigningPayload(
            method: encodedCall,
            specVersion: runtimeVersion.specVersion,
            transactionVersion: runtimeVersion.transactionVersion,
            genesisHash: encodeHex(genesisHash),
            blockHash: encodeHex(currentBlockHash),
            blockNumber: currentBlockNumber,
            eraPeriod: 64,
            nonce: nonce,
            tip: 0)
        .encode(polkadot.registry);

    final Uint8List signature = wallet.sign(payload);
    final Uint8List extrinsic = ExtrinsicPayload(
            signer: wallet.bytes(),
            method: encodedCall,
            signature: signature,
            eraPeriod: 64,
            blockNumber: currentBlockNumber,
            nonce: nonce,
            tip: 0)
        .encode(polkadot.registry, SignatureType.ed25519);

    final AuthorApi<Provider> author = AuthorApi<Provider>(provider);

    progressController.add(messageTransformer('submitting_transaction'));

    await author.submitAndWatchExtrinsic(
      extrinsic,
      (ExtrinsicStatus status) {
        final String transformedMessage = messageTransformer(status.type);
        progressController.add(transformedMessage);

        if (<String>[
          'finalized',
          'dropped',
          'invalid',
          'usurped',
        ].contains(status.type)) {
          progressController.close();
        }
      },
    ).timeout(timeout);
  } catch (e, stacktrace) {
    final String errorMessage = messageTransformer('error');
    progressController.add(errorMessage);
    loggerDev(errorMessage, error: e, stackTrace: stacktrace);
    progressController.close();
  }

  return SignAndSendResult(
    node: node,
    progressStream: progressController.stream,
  );
}

Future<String> signAndSendOld(Gdev polkadot, KeyPair wallet, RuntimeCall call,
    Provider provider, Completer<String> result, Duration timeout) async {
  final RuntimeVersion runtimeVersion =
      await polkadot.rpc.state.getRuntimeVersion();
  final int currentBlockNumber = (await polkadot.query.system.number()) - 1;
  final H256 currentBlockHash =
      await polkadot.query.system.blockHash(currentBlockNumber);
  final int nonce = await polkadot.rpc.system.accountNextIndex(wallet.address);

  final H256 genesisHash = await polkadot.query.system.blockHash(0);

  final Uint8List encodedCall = call.encode();

  final Uint8List payload = SigningPayload(
          method: encodedCall,
          specVersion: runtimeVersion.specVersion,
          transactionVersion: runtimeVersion.transactionVersion,
          genesisHash: encodeHex(genesisHash),
          blockHash: encodeHex(currentBlockHash),
          blockNumber: currentBlockNumber,
          eraPeriod: 64,
          nonce: nonce,
          tip: 0)
      .encode(polkadot.registry);

  final Uint8List signature = wallet.sign(payload);
  final Uint8List extrinsic = ExtrinsicPayload(
          signer: wallet.bytes(),
          method: encodedCall,
          signature: signature,
          eraPeriod: 64,
          blockNumber: currentBlockNumber,
          nonce: nonce,
          tip: 0)
      .encode(polkadot.registry, SignatureType.ed25519);

  final AuthorApi<Provider> author = AuthorApi<Provider>(provider);

  await author.submitAndWatchExtrinsic(extrinsic, (ExtrinsicStatus status) {
    switch (status.type) {
      case 'finalized':
        result.complete('');
        break;
      case 'dropped':
        result.complete(tr('op_dropped'));
        break;
      case 'invalid':
        result.complete(tr('op_invalid'));
        break;
      case 'usurped':
        result.complete(tr('op_usurped'));
        break;
      case 'future':
        break;
      case 'ready':
        break;
      case 'inBlock':
        break;
      case 'broadcast':
        break;
      default:
        result.complete('Unexpected transaction status: ${status.type}.');
        loggerDev('Unexpected transaction status: ${status.type}.');
        break;
    }
  }).timeout(timeout);
  return result.future;
}
