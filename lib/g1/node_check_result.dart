class NodeCheckResult {
  NodeCheckResult({required this.latency, required this.currentBlock});

  final Duration latency;
  final int currentBlock;
}
