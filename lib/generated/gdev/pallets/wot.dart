class Constants {
  Constants();

  /// The block number from which the first certification can be issued.
  final int firstIssuableOn = 0;

  /// The minimum number of certifications required for membership eligibility.
  final int minCertForMembership = 3;

  /// The minimum number of certifications required to create an identity.
  final int minCertForCreateIdtyRight = 3;
}
