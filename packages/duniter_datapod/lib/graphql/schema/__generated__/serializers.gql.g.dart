// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(FetchPolicy.serializer)
      ..add(GDeleteProfileData.serializer)
      ..add(GDeleteProfileData_deleteProfile.serializer)
      ..add(GDeleteProfileReq.serializer)
      ..add(GDeleteProfileVars.serializer)
      ..add(GGeolocInput.serializer)
      ..add(GGetProfileByAddressData.serializer)
      ..add(GGetProfileByAddressData_profiles.serializer)
      ..add(GGetProfileByAddressReq.serializer)
      ..add(GGetProfileByAddressVars.serializer)
      ..add(GGetProfileCountData.serializer)
      ..add(GGetProfileCountData_profiles_aggregate.serializer)
      ..add(GGetProfileCountData_profiles_aggregate_aggregate.serializer)
      ..add(GGetProfileCountReq.serializer)
      ..add(GGetProfileCountVars.serializer)
      ..add(GGetProfilesByAddressData.serializer)
      ..add(GGetProfilesByAddressData_profiles.serializer)
      ..add(GGetProfilesByAddressReq.serializer)
      ..add(GGetProfilesByAddressVars.serializer)
      ..add(GMigrateProfileData.serializer)
      ..add(GMigrateProfileData_migrateProfile.serializer)
      ..add(GMigrateProfileReq.serializer)
      ..add(GMigrateProfileVars.serializer)
      ..add(GSearchProfileByTermData.serializer)
      ..add(GSearchProfileByTermData_profiles.serializer)
      ..add(GSearchProfileByTermReq.serializer)
      ..add(GSearchProfileByTermVars.serializer)
      ..add(GSearchProfilesData.serializer)
      ..add(GSearchProfilesData_profiles.serializer)
      ..add(GSearchProfilesReq.serializer)
      ..add(GSearchProfilesVars.serializer)
      ..add(GSocialInput.serializer)
      ..add(GString_comparison_exp.serializer)
      ..add(GUpdateProfileData.serializer)
      ..add(GUpdateProfileData_updateProfile.serializer)
      ..add(GUpdateProfileReq.serializer)
      ..add(GUpdateProfileVars.serializer)
      ..add(Gcursor_ordering.serializer)
      ..add(Gjsonb_cast_exp.serializer)
      ..add(Gjsonb_comparison_exp.serializer)
      ..add(Gorder_by.serializer)
      ..add(Gpoint.serializer)
      ..add(Gpoint_comparison_exp.serializer)
      ..add(Gprofiles_bool_exp.serializer)
      ..add(Gprofiles_order_by.serializer)
      ..add(Gprofiles_select_column.serializer)
      ..add(Gprofiles_stream_cursor_input.serializer)
      ..add(Gprofiles_stream_cursor_value_input.serializer)
      ..add(Gtimestamp.serializer)
      ..add(Gtimestamp_comparison_exp.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GGetProfileByAddressData_profiles)]),
          () => new ListBuilder<GGetProfileByAddressData_profiles>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GGetProfilesByAddressData_profiles)]),
          () => new ListBuilder<GGetProfilesByAddressData_profiles>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GSearchProfileByTermData_profiles)]),
          () => new ListBuilder<GSearchProfileByTermData_profiles>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GSearchProfilesData_profiles)]),
          () => new ListBuilder<GSearchProfilesData_profiles>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(GSocialInput)]),
          () => new ListBuilder<GSocialInput>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Gpoint)]),
          () => new ListBuilder<Gpoint>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Gpoint)]),
          () => new ListBuilder<Gpoint>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Gprofiles_bool_exp)]),
          () => new ListBuilder<Gprofiles_bool_exp>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Gprofiles_bool_exp)]),
          () => new ListBuilder<Gprofiles_bool_exp>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Gtimestamp)]),
          () => new ListBuilder<Gtimestamp>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Gtimestamp)]),
          () => new ListBuilder<Gtimestamp>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
