// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'duniter-datapod.schema.schema.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const Gcursor_ordering _$gcursorOrderingASC = const Gcursor_ordering._('ASC');
const Gcursor_ordering _$gcursorOrderingDESC = const Gcursor_ordering._('DESC');

Gcursor_ordering _$gcursorOrderingValueOf(String name) {
  switch (name) {
    case 'ASC':
      return _$gcursorOrderingASC;
    case 'DESC':
      return _$gcursorOrderingDESC;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<Gcursor_ordering> _$gcursorOrderingValues =
    new BuiltSet<Gcursor_ordering>(const <Gcursor_ordering>[
  _$gcursorOrderingASC,
  _$gcursorOrderingDESC,
]);

const Gorder_by _$gorderByasc = const Gorder_by._('asc');
const Gorder_by _$gorderByasc_nulls_first =
    const Gorder_by._('asc_nulls_first');
const Gorder_by _$gorderByasc_nulls_last = const Gorder_by._('asc_nulls_last');
const Gorder_by _$gorderBydesc = const Gorder_by._('desc');
const Gorder_by _$gorderBydesc_nulls_first =
    const Gorder_by._('desc_nulls_first');
const Gorder_by _$gorderBydesc_nulls_last =
    const Gorder_by._('desc_nulls_last');

Gorder_by _$gorderByValueOf(String name) {
  switch (name) {
    case 'asc':
      return _$gorderByasc;
    case 'asc_nulls_first':
      return _$gorderByasc_nulls_first;
    case 'asc_nulls_last':
      return _$gorderByasc_nulls_last;
    case 'desc':
      return _$gorderBydesc;
    case 'desc_nulls_first':
      return _$gorderBydesc_nulls_first;
    case 'desc_nulls_last':
      return _$gorderBydesc_nulls_last;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<Gorder_by> _$gorderByValues =
    new BuiltSet<Gorder_by>(const <Gorder_by>[
  _$gorderByasc,
  _$gorderByasc_nulls_first,
  _$gorderByasc_nulls_last,
  _$gorderBydesc,
  _$gorderBydesc_nulls_first,
  _$gorderBydesc_nulls_last,
]);

const Gprofiles_select_column _$gprofilesSelectColumnavatar =
    const Gprofiles_select_column._('avatar');
const Gprofiles_select_column _$gprofilesSelectColumncity =
    const Gprofiles_select_column._('city');
const Gprofiles_select_column _$gprofilesSelectColumndata_cid =
    const Gprofiles_select_column._('data_cid');
const Gprofiles_select_column _$gprofilesSelectColumndescription =
    const Gprofiles_select_column._('description');
const Gprofiles_select_column _$gprofilesSelectColumngeoloc =
    const Gprofiles_select_column._('geoloc');
const Gprofiles_select_column _$gprofilesSelectColumnindex_request_cid =
    const Gprofiles_select_column._('index_request_cid');
const Gprofiles_select_column _$gprofilesSelectColumnpubkey =
    const Gprofiles_select_column._('pubkey');
const Gprofiles_select_column _$gprofilesSelectColumnsocials =
    const Gprofiles_select_column._('socials');
const Gprofiles_select_column _$gprofilesSelectColumntime =
    const Gprofiles_select_column._('time');
const Gprofiles_select_column _$gprofilesSelectColumntitle =
    const Gprofiles_select_column._('title');

Gprofiles_select_column _$gprofilesSelectColumnValueOf(String name) {
  switch (name) {
    case 'avatar':
      return _$gprofilesSelectColumnavatar;
    case 'city':
      return _$gprofilesSelectColumncity;
    case 'data_cid':
      return _$gprofilesSelectColumndata_cid;
    case 'description':
      return _$gprofilesSelectColumndescription;
    case 'geoloc':
      return _$gprofilesSelectColumngeoloc;
    case 'index_request_cid':
      return _$gprofilesSelectColumnindex_request_cid;
    case 'pubkey':
      return _$gprofilesSelectColumnpubkey;
    case 'socials':
      return _$gprofilesSelectColumnsocials;
    case 'time':
      return _$gprofilesSelectColumntime;
    case 'title':
      return _$gprofilesSelectColumntitle;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<Gprofiles_select_column> _$gprofilesSelectColumnValues =
    new BuiltSet<Gprofiles_select_column>(const <Gprofiles_select_column>[
  _$gprofilesSelectColumnavatar,
  _$gprofilesSelectColumncity,
  _$gprofilesSelectColumndata_cid,
  _$gprofilesSelectColumndescription,
  _$gprofilesSelectColumngeoloc,
  _$gprofilesSelectColumnindex_request_cid,
  _$gprofilesSelectColumnpubkey,
  _$gprofilesSelectColumnsocials,
  _$gprofilesSelectColumntime,
  _$gprofilesSelectColumntitle,
]);

Serializer<Gcursor_ordering> _$gcursorOrderingSerializer =
    new _$Gcursor_orderingSerializer();
Serializer<GGeolocInput> _$gGeolocInputSerializer =
    new _$GGeolocInputSerializer();
Serializer<Gjsonb_cast_exp> _$gjsonbCastExpSerializer =
    new _$Gjsonb_cast_expSerializer();
Serializer<Gjsonb_comparison_exp> _$gjsonbComparisonExpSerializer =
    new _$Gjsonb_comparison_expSerializer();
Serializer<Gorder_by> _$gorderBySerializer = new _$Gorder_bySerializer();
Serializer<Gpoint_comparison_exp> _$gpointComparisonExpSerializer =
    new _$Gpoint_comparison_expSerializer();
Serializer<Gprofiles_bool_exp> _$gprofilesBoolExpSerializer =
    new _$Gprofiles_bool_expSerializer();
Serializer<Gprofiles_order_by> _$gprofilesOrderBySerializer =
    new _$Gprofiles_order_bySerializer();
Serializer<Gprofiles_select_column> _$gprofilesSelectColumnSerializer =
    new _$Gprofiles_select_columnSerializer();
Serializer<Gprofiles_stream_cursor_input>
    _$gprofilesStreamCursorInputSerializer =
    new _$Gprofiles_stream_cursor_inputSerializer();
Serializer<Gprofiles_stream_cursor_value_input>
    _$gprofilesStreamCursorValueInputSerializer =
    new _$Gprofiles_stream_cursor_value_inputSerializer();
Serializer<GSocialInput> _$gSocialInputSerializer =
    new _$GSocialInputSerializer();
Serializer<GString_comparison_exp> _$gStringComparisonExpSerializer =
    new _$GString_comparison_expSerializer();
Serializer<Gtimestamp_comparison_exp> _$gtimestampComparisonExpSerializer =
    new _$Gtimestamp_comparison_expSerializer();

class _$Gcursor_orderingSerializer
    implements PrimitiveSerializer<Gcursor_ordering> {
  @override
  final Iterable<Type> types = const <Type>[Gcursor_ordering];
  @override
  final String wireName = 'Gcursor_ordering';

  @override
  Object serialize(Serializers serializers, Gcursor_ordering object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  Gcursor_ordering deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      Gcursor_ordering.valueOf(serialized as String);
}

class _$GGeolocInputSerializer implements StructuredSerializer<GGeolocInput> {
  @override
  final Iterable<Type> types = const [GGeolocInput, _$GGeolocInput];
  @override
  final String wireName = 'GGeolocInput';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGeolocInput object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'latitude',
      serializers.serialize(object.latitude,
          specifiedType: const FullType(double)),
      'longitude',
      serializers.serialize(object.longitude,
          specifiedType: const FullType(double)),
    ];

    return result;
  }

  @override
  GGeolocInput deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGeolocInputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
      }
    }

    return result.build();
  }
}

class _$Gjsonb_cast_expSerializer
    implements StructuredSerializer<Gjsonb_cast_exp> {
  @override
  final Iterable<Type> types = const [Gjsonb_cast_exp, _$Gjsonb_cast_exp];
  @override
  final String wireName = 'Gjsonb_cast_exp';

  @override
  Iterable<Object?> serialize(Serializers serializers, Gjsonb_cast_exp object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.GString;
    if (value != null) {
      result
        ..add('String')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    return result;
  }

  @override
  Gjsonb_cast_exp deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gjsonb_cast_expBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'String':
          result.GString.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
      }
    }

    return result.build();
  }
}

class _$Gjsonb_comparison_expSerializer
    implements StructuredSerializer<Gjsonb_comparison_exp> {
  @override
  final Iterable<Type> types = const [
    Gjsonb_comparison_exp,
    _$Gjsonb_comparison_exp
  ];
  @override
  final String wireName = 'Gjsonb_comparison_exp';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, Gjsonb_comparison_exp object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.G_cast;
    if (value != null) {
      result
        ..add('_cast')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gjsonb_cast_exp)));
    }
    value = object.G_contained_in;
    if (value != null) {
      result
        ..add('_contained_in')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_contains;
    if (value != null) {
      result
        ..add('_contains')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_eq;
    if (value != null) {
      result
        ..add('_eq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_gt;
    if (value != null) {
      result
        ..add('_gt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_gte;
    if (value != null) {
      result
        ..add('_gte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_has_key;
    if (value != null) {
      result
        ..add('_has_key')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_has_keys_all;
    if (value != null) {
      result
        ..add('_has_keys_all')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.G_has_keys_any;
    if (value != null) {
      result
        ..add('_has_keys_any')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.G_in;
    if (value != null) {
      result
        ..add('_in')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(_i2.JsonObject)])));
    }
    value = object.G_is_null;
    if (value != null) {
      result
        ..add('_is_null')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.G_lt;
    if (value != null) {
      result
        ..add('_lt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_lte;
    if (value != null) {
      result
        ..add('_lte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_neq;
    if (value != null) {
      result
        ..add('_neq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.G_nin;
    if (value != null) {
      result
        ..add('_nin')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(_i2.JsonObject)])));
    }
    return result;
  }

  @override
  Gjsonb_comparison_exp deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gjsonb_comparison_expBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '_cast':
          result.G_cast.replace(serializers.deserialize(value,
                  specifiedType: const FullType(Gjsonb_cast_exp))!
              as Gjsonb_cast_exp);
          break;
        case '_contained_in':
          result.G_contained_in = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_contains':
          result.G_contains = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_eq':
          result.G_eq = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_gt':
          result.G_gt = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_gte':
          result.G_gte = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_has_key':
          result.G_has_key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_has_keys_all':
          result.G_has_keys_all.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case '_has_keys_any':
          result.G_has_keys_any.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case '_in':
          result.G_in.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(_i2.JsonObject)]))!
              as BuiltList<Object?>);
          break;
        case '_is_null':
          result.G_is_null = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case '_lt':
          result.G_lt = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_lte':
          result.G_lte = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_neq':
          result.G_neq = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case '_nin':
          result.G_nin.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(_i2.JsonObject)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$Gorder_bySerializer implements PrimitiveSerializer<Gorder_by> {
  @override
  final Iterable<Type> types = const <Type>[Gorder_by];
  @override
  final String wireName = 'Gorder_by';

  @override
  Object serialize(Serializers serializers, Gorder_by object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  Gorder_by deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      Gorder_by.valueOf(serialized as String);
}

class _$Gpoint_comparison_expSerializer
    implements StructuredSerializer<Gpoint_comparison_exp> {
  @override
  final Iterable<Type> types = const [
    Gpoint_comparison_exp,
    _$Gpoint_comparison_exp
  ];
  @override
  final String wireName = 'Gpoint_comparison_exp';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, Gpoint_comparison_exp object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.G_eq;
    if (value != null) {
      result
        ..add('_eq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint)));
    }
    value = object.G_gt;
    if (value != null) {
      result
        ..add('_gt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint)));
    }
    value = object.G_gte;
    if (value != null) {
      result
        ..add('_gte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint)));
    }
    value = object.G_in;
    if (value != null) {
      result
        ..add('_in')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Gpoint)])));
    }
    value = object.G_is_null;
    if (value != null) {
      result
        ..add('_is_null')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.G_lt;
    if (value != null) {
      result
        ..add('_lt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint)));
    }
    value = object.G_lte;
    if (value != null) {
      result
        ..add('_lte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint)));
    }
    value = object.G_neq;
    if (value != null) {
      result
        ..add('_neq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint)));
    }
    value = object.G_nin;
    if (value != null) {
      result
        ..add('_nin')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Gpoint)])));
    }
    return result;
  }

  @override
  Gpoint_comparison_exp deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gpoint_comparison_expBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '_eq':
          result.G_eq.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gpoint))! as Gpoint);
          break;
        case '_gt':
          result.G_gt.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gpoint))! as Gpoint);
          break;
        case '_gte':
          result.G_gte.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gpoint))! as Gpoint);
          break;
        case '_in':
          result.G_in.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Gpoint)]))!
              as BuiltList<Object?>);
          break;
        case '_is_null':
          result.G_is_null = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case '_lt':
          result.G_lt.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gpoint))! as Gpoint);
          break;
        case '_lte':
          result.G_lte.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gpoint))! as Gpoint);
          break;
        case '_neq':
          result.G_neq.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gpoint))! as Gpoint);
          break;
        case '_nin':
          result.G_nin.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Gpoint)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$Gprofiles_bool_expSerializer
    implements StructuredSerializer<Gprofiles_bool_exp> {
  @override
  final Iterable<Type> types = const [Gprofiles_bool_exp, _$Gprofiles_bool_exp];
  @override
  final String wireName = 'Gprofiles_bool_exp';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, Gprofiles_bool_exp object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.G_and;
    if (value != null) {
      result
        ..add('_and')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(Gprofiles_bool_exp)])));
    }
    value = object.G_not;
    if (value != null) {
      result
        ..add('_not')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gprofiles_bool_exp)));
    }
    value = object.G_or;
    if (value != null) {
      result
        ..add('_or')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(Gprofiles_bool_exp)])));
    }
    value = object.avatar;
    if (value != null) {
      result
        ..add('avatar')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    value = object.city;
    if (value != null) {
      result
        ..add('city')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    value = object.data_cid;
    if (value != null) {
      result
        ..add('data_cid')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    value = object.description;
    if (value != null) {
      result
        ..add('description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    value = object.geoloc;
    if (value != null) {
      result
        ..add('geoloc')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint_comparison_exp)));
    }
    value = object.index_request_cid;
    if (value != null) {
      result
        ..add('index_request_cid')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    value = object.pubkey;
    if (value != null) {
      result
        ..add('pubkey')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    value = object.socials;
    if (value != null) {
      result
        ..add('socials')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gjsonb_comparison_exp)));
    }
    value = object.time;
    if (value != null) {
      result
        ..add('time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp_comparison_exp)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GString_comparison_exp)));
    }
    return result;
  }

  @override
  Gprofiles_bool_exp deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gprofiles_bool_expBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '_and':
          result.G_and.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Gprofiles_bool_exp)]))!
              as BuiltList<Object?>);
          break;
        case '_not':
          result.G_not.replace(serializers.deserialize(value,
                  specifiedType: const FullType(Gprofiles_bool_exp))!
              as Gprofiles_bool_exp);
          break;
        case '_or':
          result.G_or.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Gprofiles_bool_exp)]))!
              as BuiltList<Object?>);
          break;
        case 'avatar':
          result.avatar.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
        case 'city':
          result.city.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
        case 'data_cid':
          result.data_cid.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
        case 'description':
          result.description.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
        case 'geoloc':
          result.geoloc.replace(serializers.deserialize(value,
                  specifiedType: const FullType(Gpoint_comparison_exp))!
              as Gpoint_comparison_exp);
          break;
        case 'index_request_cid':
          result.index_request_cid.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
        case 'pubkey':
          result.pubkey.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
        case 'socials':
          result.socials.replace(serializers.deserialize(value,
                  specifiedType: const FullType(Gjsonb_comparison_exp))!
              as Gjsonb_comparison_exp);
          break;
        case 'time':
          result.time.replace(serializers.deserialize(value,
                  specifiedType: const FullType(Gtimestamp_comparison_exp))!
              as Gtimestamp_comparison_exp);
          break;
        case 'title':
          result.title.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GString_comparison_exp))!
              as GString_comparison_exp);
          break;
      }
    }

    return result.build();
  }
}

class _$Gprofiles_order_bySerializer
    implements StructuredSerializer<Gprofiles_order_by> {
  @override
  final Iterable<Type> types = const [Gprofiles_order_by, _$Gprofiles_order_by];
  @override
  final String wireName = 'Gprofiles_order_by';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, Gprofiles_order_by object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.avatar;
    if (value != null) {
      result
        ..add('avatar')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.city;
    if (value != null) {
      result
        ..add('city')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.data_cid;
    if (value != null) {
      result
        ..add('data_cid')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.description;
    if (value != null) {
      result
        ..add('description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.geoloc;
    if (value != null) {
      result
        ..add('geoloc')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.index_request_cid;
    if (value != null) {
      result
        ..add('index_request_cid')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.pubkey;
    if (value != null) {
      result
        ..add('pubkey')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.socials;
    if (value != null) {
      result
        ..add('socials')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.time;
    if (value != null) {
      result
        ..add('time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gorder_by)));
    }
    return result;
  }

  @override
  Gprofiles_order_by deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gprofiles_order_byBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'avatar':
          result.avatar = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'data_cid':
          result.data_cid = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'geoloc':
          result.geoloc = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'index_request_cid':
          result.index_request_cid = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'pubkey':
          result.pubkey = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'socials':
          result.socials = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'time':
          result.time = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(Gorder_by)) as Gorder_by?;
          break;
      }
    }

    return result.build();
  }
}

class _$Gprofiles_select_columnSerializer
    implements PrimitiveSerializer<Gprofiles_select_column> {
  @override
  final Iterable<Type> types = const <Type>[Gprofiles_select_column];
  @override
  final String wireName = 'Gprofiles_select_column';

  @override
  Object serialize(Serializers serializers, Gprofiles_select_column object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  Gprofiles_select_column deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      Gprofiles_select_column.valueOf(serialized as String);
}

class _$Gprofiles_stream_cursor_inputSerializer
    implements StructuredSerializer<Gprofiles_stream_cursor_input> {
  @override
  final Iterable<Type> types = const [
    Gprofiles_stream_cursor_input,
    _$Gprofiles_stream_cursor_input
  ];
  @override
  final String wireName = 'Gprofiles_stream_cursor_input';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, Gprofiles_stream_cursor_input object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'initial_value',
      serializers.serialize(object.initial_value,
          specifiedType: const FullType(Gprofiles_stream_cursor_value_input)),
    ];
    Object? value;
    value = object.ordering;
    if (value != null) {
      result
        ..add('ordering')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gcursor_ordering)));
    }
    return result;
  }

  @override
  Gprofiles_stream_cursor_input deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gprofiles_stream_cursor_inputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'initial_value':
          result.initial_value.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(Gprofiles_stream_cursor_value_input))!
              as Gprofiles_stream_cursor_value_input);
          break;
        case 'ordering':
          result.ordering = serializers.deserialize(value,
                  specifiedType: const FullType(Gcursor_ordering))
              as Gcursor_ordering?;
          break;
      }
    }

    return result.build();
  }
}

class _$Gprofiles_stream_cursor_value_inputSerializer
    implements StructuredSerializer<Gprofiles_stream_cursor_value_input> {
  @override
  final Iterable<Type> types = const [
    Gprofiles_stream_cursor_value_input,
    _$Gprofiles_stream_cursor_value_input
  ];
  @override
  final String wireName = 'Gprofiles_stream_cursor_value_input';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, Gprofiles_stream_cursor_value_input object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.avatar;
    if (value != null) {
      result
        ..add('avatar')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.city;
    if (value != null) {
      result
        ..add('city')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.data_cid;
    if (value != null) {
      result
        ..add('data_cid')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.description;
    if (value != null) {
      result
        ..add('description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.geoloc;
    if (value != null) {
      result
        ..add('geoloc')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gpoint)));
    }
    value = object.index_request_cid;
    if (value != null) {
      result
        ..add('index_request_cid')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.pubkey;
    if (value != null) {
      result
        ..add('pubkey')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.socials;
    if (value != null) {
      result
        ..add('socials')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.JsonObject)));
    }
    value = object.time;
    if (value != null) {
      result
        ..add('time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Gprofiles_stream_cursor_value_input deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gprofiles_stream_cursor_value_inputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'avatar':
          result.avatar = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'data_cid':
          result.data_cid = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'geoloc':
          result.geoloc.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gpoint))! as Gpoint);
          break;
        case 'index_request_cid':
          result.index_request_cid = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pubkey':
          result.pubkey = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'socials':
          result.socials = serializers.deserialize(value,
              specifiedType: const FullType(_i2.JsonObject)) as _i2.JsonObject?;
          break;
        case 'time':
          result.time.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gtimestamp))! as Gtimestamp);
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GSocialInputSerializer implements StructuredSerializer<GSocialInput> {
  @override
  final Iterable<Type> types = const [GSocialInput, _$GSocialInput];
  @override
  final String wireName = 'GSocialInput';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSocialInput object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'url',
      serializers.serialize(object.url, specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.type;
    if (value != null) {
      result
        ..add('type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GSocialInput deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSocialInputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GString_comparison_expSerializer
    implements StructuredSerializer<GString_comparison_exp> {
  @override
  final Iterable<Type> types = const [
    GString_comparison_exp,
    _$GString_comparison_exp
  ];
  @override
  final String wireName = 'GString_comparison_exp';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GString_comparison_exp object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.G_eq;
    if (value != null) {
      result
        ..add('_eq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_gt;
    if (value != null) {
      result
        ..add('_gt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_gte;
    if (value != null) {
      result
        ..add('_gte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_ilike;
    if (value != null) {
      result
        ..add('_ilike')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_in;
    if (value != null) {
      result
        ..add('_in')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.G_iregex;
    if (value != null) {
      result
        ..add('_iregex')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_is_null;
    if (value != null) {
      result
        ..add('_is_null')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.G_like;
    if (value != null) {
      result
        ..add('_like')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_lt;
    if (value != null) {
      result
        ..add('_lt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_lte;
    if (value != null) {
      result
        ..add('_lte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_neq;
    if (value != null) {
      result
        ..add('_neq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_nilike;
    if (value != null) {
      result
        ..add('_nilike')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_nin;
    if (value != null) {
      result
        ..add('_nin')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.G_niregex;
    if (value != null) {
      result
        ..add('_niregex')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_nlike;
    if (value != null) {
      result
        ..add('_nlike')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_nregex;
    if (value != null) {
      result
        ..add('_nregex')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_nsimilar;
    if (value != null) {
      result
        ..add('_nsimilar')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_regex;
    if (value != null) {
      result
        ..add('_regex')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.G_similar;
    if (value != null) {
      result
        ..add('_similar')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GString_comparison_exp deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GString_comparison_expBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '_eq':
          result.G_eq = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_gt':
          result.G_gt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_gte':
          result.G_gte = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_ilike':
          result.G_ilike = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_in':
          result.G_in.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case '_iregex':
          result.G_iregex = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_is_null':
          result.G_is_null = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case '_like':
          result.G_like = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_lt':
          result.G_lt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_lte':
          result.G_lte = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_neq':
          result.G_neq = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_nilike':
          result.G_nilike = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_nin':
          result.G_nin.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case '_niregex':
          result.G_niregex = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_nlike':
          result.G_nlike = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_nregex':
          result.G_nregex = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_nsimilar':
          result.G_nsimilar = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_regex':
          result.G_regex = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case '_similar':
          result.G_similar = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$Gtimestamp_comparison_expSerializer
    implements StructuredSerializer<Gtimestamp_comparison_exp> {
  @override
  final Iterable<Type> types = const [
    Gtimestamp_comparison_exp,
    _$Gtimestamp_comparison_exp
  ];
  @override
  final String wireName = 'Gtimestamp_comparison_exp';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, Gtimestamp_comparison_exp object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.G_eq;
    if (value != null) {
      result
        ..add('_eq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp)));
    }
    value = object.G_gt;
    if (value != null) {
      result
        ..add('_gt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp)));
    }
    value = object.G_gte;
    if (value != null) {
      result
        ..add('_gte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp)));
    }
    value = object.G_in;
    if (value != null) {
      result
        ..add('_in')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Gtimestamp)])));
    }
    value = object.G_is_null;
    if (value != null) {
      result
        ..add('_is_null')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.G_lt;
    if (value != null) {
      result
        ..add('_lt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp)));
    }
    value = object.G_lte;
    if (value != null) {
      result
        ..add('_lte')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp)));
    }
    value = object.G_neq;
    if (value != null) {
      result
        ..add('_neq')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Gtimestamp)));
    }
    value = object.G_nin;
    if (value != null) {
      result
        ..add('_nin')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Gtimestamp)])));
    }
    return result;
  }

  @override
  Gtimestamp_comparison_exp deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Gtimestamp_comparison_expBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '_eq':
          result.G_eq.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gtimestamp))! as Gtimestamp);
          break;
        case '_gt':
          result.G_gt.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gtimestamp))! as Gtimestamp);
          break;
        case '_gte':
          result.G_gte.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gtimestamp))! as Gtimestamp);
          break;
        case '_in':
          result.G_in.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Gtimestamp)]))!
              as BuiltList<Object?>);
          break;
        case '_is_null':
          result.G_is_null = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case '_lt':
          result.G_lt.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gtimestamp))! as Gtimestamp);
          break;
        case '_lte':
          result.G_lte.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gtimestamp))! as Gtimestamp);
          break;
        case '_neq':
          result.G_neq.replace(serializers.deserialize(value,
              specifiedType: const FullType(Gtimestamp))! as Gtimestamp);
          break;
        case '_nin':
          result.G_nin.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Gtimestamp)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GGeolocInput extends GGeolocInput {
  @override
  final double latitude;
  @override
  final double longitude;

  factory _$GGeolocInput([void Function(GGeolocInputBuilder)? updates]) =>
      (new GGeolocInputBuilder()..update(updates))._build();

  _$GGeolocInput._({required this.latitude, required this.longitude})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        latitude, r'GGeolocInput', 'latitude');
    BuiltValueNullFieldError.checkNotNull(
        longitude, r'GGeolocInput', 'longitude');
  }

  @override
  GGeolocInput rebuild(void Function(GGeolocInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGeolocInputBuilder toBuilder() => new GGeolocInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGeolocInput &&
        latitude == other.latitude &&
        longitude == other.longitude;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GGeolocInput')
          ..add('latitude', latitude)
          ..add('longitude', longitude))
        .toString();
  }
}

class GGeolocInputBuilder
    implements Builder<GGeolocInput, GGeolocInputBuilder> {
  _$GGeolocInput? _$v;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  GGeolocInputBuilder();

  GGeolocInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGeolocInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGeolocInput;
  }

  @override
  void update(void Function(GGeolocInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GGeolocInput build() => _build();

  _$GGeolocInput _build() {
    final _$result = _$v ??
        new _$GGeolocInput._(
            latitude: BuiltValueNullFieldError.checkNotNull(
                latitude, r'GGeolocInput', 'latitude'),
            longitude: BuiltValueNullFieldError.checkNotNull(
                longitude, r'GGeolocInput', 'longitude'));
    replace(_$result);
    return _$result;
  }
}

class _$Gjsonb_cast_exp extends Gjsonb_cast_exp {
  @override
  final GString_comparison_exp? GString;

  factory _$Gjsonb_cast_exp([void Function(Gjsonb_cast_expBuilder)? updates]) =>
      (new Gjsonb_cast_expBuilder()..update(updates))._build();

  _$Gjsonb_cast_exp._({this.GString}) : super._();

  @override
  Gjsonb_cast_exp rebuild(void Function(Gjsonb_cast_expBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gjsonb_cast_expBuilder toBuilder() =>
      new Gjsonb_cast_expBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gjsonb_cast_exp && GString == other.GString;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, GString.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gjsonb_cast_exp')
          ..add('GString', GString))
        .toString();
  }
}

class Gjsonb_cast_expBuilder
    implements Builder<Gjsonb_cast_exp, Gjsonb_cast_expBuilder> {
  _$Gjsonb_cast_exp? _$v;

  GString_comparison_expBuilder? _GString;
  GString_comparison_expBuilder get GString =>
      _$this._GString ??= new GString_comparison_expBuilder();
  set GString(GString_comparison_expBuilder? GString) =>
      _$this._GString = GString;

  Gjsonb_cast_expBuilder();

  Gjsonb_cast_expBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _GString = $v.GString?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gjsonb_cast_exp other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gjsonb_cast_exp;
  }

  @override
  void update(void Function(Gjsonb_cast_expBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gjsonb_cast_exp build() => _build();

  _$Gjsonb_cast_exp _build() {
    _$Gjsonb_cast_exp _$result;
    try {
      _$result = _$v ?? new _$Gjsonb_cast_exp._(GString: _GString?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'GString';
        _GString?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Gjsonb_cast_exp', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Gjsonb_comparison_exp extends Gjsonb_comparison_exp {
  @override
  final Gjsonb_cast_exp? G_cast;
  @override
  final _i2.JsonObject? G_contained_in;
  @override
  final _i2.JsonObject? G_contains;
  @override
  final _i2.JsonObject? G_eq;
  @override
  final _i2.JsonObject? G_gt;
  @override
  final _i2.JsonObject? G_gte;
  @override
  final String? G_has_key;
  @override
  final BuiltList<String>? G_has_keys_all;
  @override
  final BuiltList<String>? G_has_keys_any;
  @override
  final BuiltList<_i2.JsonObject>? G_in;
  @override
  final bool? G_is_null;
  @override
  final _i2.JsonObject? G_lt;
  @override
  final _i2.JsonObject? G_lte;
  @override
  final _i2.JsonObject? G_neq;
  @override
  final BuiltList<_i2.JsonObject>? G_nin;

  factory _$Gjsonb_comparison_exp(
          [void Function(Gjsonb_comparison_expBuilder)? updates]) =>
      (new Gjsonb_comparison_expBuilder()..update(updates))._build();

  _$Gjsonb_comparison_exp._(
      {this.G_cast,
      this.G_contained_in,
      this.G_contains,
      this.G_eq,
      this.G_gt,
      this.G_gte,
      this.G_has_key,
      this.G_has_keys_all,
      this.G_has_keys_any,
      this.G_in,
      this.G_is_null,
      this.G_lt,
      this.G_lte,
      this.G_neq,
      this.G_nin})
      : super._();

  @override
  Gjsonb_comparison_exp rebuild(
          void Function(Gjsonb_comparison_expBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gjsonb_comparison_expBuilder toBuilder() =>
      new Gjsonb_comparison_expBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gjsonb_comparison_exp &&
        G_cast == other.G_cast &&
        G_contained_in == other.G_contained_in &&
        G_contains == other.G_contains &&
        G_eq == other.G_eq &&
        G_gt == other.G_gt &&
        G_gte == other.G_gte &&
        G_has_key == other.G_has_key &&
        G_has_keys_all == other.G_has_keys_all &&
        G_has_keys_any == other.G_has_keys_any &&
        G_in == other.G_in &&
        G_is_null == other.G_is_null &&
        G_lt == other.G_lt &&
        G_lte == other.G_lte &&
        G_neq == other.G_neq &&
        G_nin == other.G_nin;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, G_cast.hashCode);
    _$hash = $jc(_$hash, G_contained_in.hashCode);
    _$hash = $jc(_$hash, G_contains.hashCode);
    _$hash = $jc(_$hash, G_eq.hashCode);
    _$hash = $jc(_$hash, G_gt.hashCode);
    _$hash = $jc(_$hash, G_gte.hashCode);
    _$hash = $jc(_$hash, G_has_key.hashCode);
    _$hash = $jc(_$hash, G_has_keys_all.hashCode);
    _$hash = $jc(_$hash, G_has_keys_any.hashCode);
    _$hash = $jc(_$hash, G_in.hashCode);
    _$hash = $jc(_$hash, G_is_null.hashCode);
    _$hash = $jc(_$hash, G_lt.hashCode);
    _$hash = $jc(_$hash, G_lte.hashCode);
    _$hash = $jc(_$hash, G_neq.hashCode);
    _$hash = $jc(_$hash, G_nin.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gjsonb_comparison_exp')
          ..add('G_cast', G_cast)
          ..add('G_contained_in', G_contained_in)
          ..add('G_contains', G_contains)
          ..add('G_eq', G_eq)
          ..add('G_gt', G_gt)
          ..add('G_gte', G_gte)
          ..add('G_has_key', G_has_key)
          ..add('G_has_keys_all', G_has_keys_all)
          ..add('G_has_keys_any', G_has_keys_any)
          ..add('G_in', G_in)
          ..add('G_is_null', G_is_null)
          ..add('G_lt', G_lt)
          ..add('G_lte', G_lte)
          ..add('G_neq', G_neq)
          ..add('G_nin', G_nin))
        .toString();
  }
}

class Gjsonb_comparison_expBuilder
    implements Builder<Gjsonb_comparison_exp, Gjsonb_comparison_expBuilder> {
  _$Gjsonb_comparison_exp? _$v;

  Gjsonb_cast_expBuilder? _G_cast;
  Gjsonb_cast_expBuilder get G_cast =>
      _$this._G_cast ??= new Gjsonb_cast_expBuilder();
  set G_cast(Gjsonb_cast_expBuilder? G_cast) => _$this._G_cast = G_cast;

  _i2.JsonObject? _G_contained_in;
  _i2.JsonObject? get G_contained_in => _$this._G_contained_in;
  set G_contained_in(_i2.JsonObject? G_contained_in) =>
      _$this._G_contained_in = G_contained_in;

  _i2.JsonObject? _G_contains;
  _i2.JsonObject? get G_contains => _$this._G_contains;
  set G_contains(_i2.JsonObject? G_contains) => _$this._G_contains = G_contains;

  _i2.JsonObject? _G_eq;
  _i2.JsonObject? get G_eq => _$this._G_eq;
  set G_eq(_i2.JsonObject? G_eq) => _$this._G_eq = G_eq;

  _i2.JsonObject? _G_gt;
  _i2.JsonObject? get G_gt => _$this._G_gt;
  set G_gt(_i2.JsonObject? G_gt) => _$this._G_gt = G_gt;

  _i2.JsonObject? _G_gte;
  _i2.JsonObject? get G_gte => _$this._G_gte;
  set G_gte(_i2.JsonObject? G_gte) => _$this._G_gte = G_gte;

  String? _G_has_key;
  String? get G_has_key => _$this._G_has_key;
  set G_has_key(String? G_has_key) => _$this._G_has_key = G_has_key;

  ListBuilder<String>? _G_has_keys_all;
  ListBuilder<String> get G_has_keys_all =>
      _$this._G_has_keys_all ??= new ListBuilder<String>();
  set G_has_keys_all(ListBuilder<String>? G_has_keys_all) =>
      _$this._G_has_keys_all = G_has_keys_all;

  ListBuilder<String>? _G_has_keys_any;
  ListBuilder<String> get G_has_keys_any =>
      _$this._G_has_keys_any ??= new ListBuilder<String>();
  set G_has_keys_any(ListBuilder<String>? G_has_keys_any) =>
      _$this._G_has_keys_any = G_has_keys_any;

  ListBuilder<_i2.JsonObject>? _G_in;
  ListBuilder<_i2.JsonObject> get G_in =>
      _$this._G_in ??= new ListBuilder<_i2.JsonObject>();
  set G_in(ListBuilder<_i2.JsonObject>? G_in) => _$this._G_in = G_in;

  bool? _G_is_null;
  bool? get G_is_null => _$this._G_is_null;
  set G_is_null(bool? G_is_null) => _$this._G_is_null = G_is_null;

  _i2.JsonObject? _G_lt;
  _i2.JsonObject? get G_lt => _$this._G_lt;
  set G_lt(_i2.JsonObject? G_lt) => _$this._G_lt = G_lt;

  _i2.JsonObject? _G_lte;
  _i2.JsonObject? get G_lte => _$this._G_lte;
  set G_lte(_i2.JsonObject? G_lte) => _$this._G_lte = G_lte;

  _i2.JsonObject? _G_neq;
  _i2.JsonObject? get G_neq => _$this._G_neq;
  set G_neq(_i2.JsonObject? G_neq) => _$this._G_neq = G_neq;

  ListBuilder<_i2.JsonObject>? _G_nin;
  ListBuilder<_i2.JsonObject> get G_nin =>
      _$this._G_nin ??= new ListBuilder<_i2.JsonObject>();
  set G_nin(ListBuilder<_i2.JsonObject>? G_nin) => _$this._G_nin = G_nin;

  Gjsonb_comparison_expBuilder();

  Gjsonb_comparison_expBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G_cast = $v.G_cast?.toBuilder();
      _G_contained_in = $v.G_contained_in;
      _G_contains = $v.G_contains;
      _G_eq = $v.G_eq;
      _G_gt = $v.G_gt;
      _G_gte = $v.G_gte;
      _G_has_key = $v.G_has_key;
      _G_has_keys_all = $v.G_has_keys_all?.toBuilder();
      _G_has_keys_any = $v.G_has_keys_any?.toBuilder();
      _G_in = $v.G_in?.toBuilder();
      _G_is_null = $v.G_is_null;
      _G_lt = $v.G_lt;
      _G_lte = $v.G_lte;
      _G_neq = $v.G_neq;
      _G_nin = $v.G_nin?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gjsonb_comparison_exp other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gjsonb_comparison_exp;
  }

  @override
  void update(void Function(Gjsonb_comparison_expBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gjsonb_comparison_exp build() => _build();

  _$Gjsonb_comparison_exp _build() {
    _$Gjsonb_comparison_exp _$result;
    try {
      _$result = _$v ??
          new _$Gjsonb_comparison_exp._(
              G_cast: _G_cast?.build(),
              G_contained_in: G_contained_in,
              G_contains: G_contains,
              G_eq: G_eq,
              G_gt: G_gt,
              G_gte: G_gte,
              G_has_key: G_has_key,
              G_has_keys_all: _G_has_keys_all?.build(),
              G_has_keys_any: _G_has_keys_any?.build(),
              G_in: _G_in?.build(),
              G_is_null: G_is_null,
              G_lt: G_lt,
              G_lte: G_lte,
              G_neq: G_neq,
              G_nin: _G_nin?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'G_cast';
        _G_cast?.build();

        _$failedField = 'G_has_keys_all';
        _G_has_keys_all?.build();
        _$failedField = 'G_has_keys_any';
        _G_has_keys_any?.build();
        _$failedField = 'G_in';
        _G_in?.build();

        _$failedField = 'G_nin';
        _G_nin?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Gjsonb_comparison_exp', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Gpoint extends Gpoint {
  @override
  final String value;

  factory _$Gpoint([void Function(GpointBuilder)? updates]) =>
      (new GpointBuilder()..update(updates))._build();

  _$Gpoint._({required this.value}) : super._() {
    BuiltValueNullFieldError.checkNotNull(value, r'Gpoint', 'value');
  }

  @override
  Gpoint rebuild(void Function(GpointBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GpointBuilder toBuilder() => new GpointBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gpoint && value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gpoint')..add('value', value))
        .toString();
  }
}

class GpointBuilder implements Builder<Gpoint, GpointBuilder> {
  _$Gpoint? _$v;

  String? _value;
  String? get value => _$this._value;
  set value(String? value) => _$this._value = value;

  GpointBuilder();

  GpointBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gpoint other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gpoint;
  }

  @override
  void update(void Function(GpointBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gpoint build() => _build();

  _$Gpoint _build() {
    final _$result = _$v ??
        new _$Gpoint._(
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'Gpoint', 'value'));
    replace(_$result);
    return _$result;
  }
}

class _$Gpoint_comparison_exp extends Gpoint_comparison_exp {
  @override
  final Gpoint? G_eq;
  @override
  final Gpoint? G_gt;
  @override
  final Gpoint? G_gte;
  @override
  final BuiltList<Gpoint>? G_in;
  @override
  final bool? G_is_null;
  @override
  final Gpoint? G_lt;
  @override
  final Gpoint? G_lte;
  @override
  final Gpoint? G_neq;
  @override
  final BuiltList<Gpoint>? G_nin;

  factory _$Gpoint_comparison_exp(
          [void Function(Gpoint_comparison_expBuilder)? updates]) =>
      (new Gpoint_comparison_expBuilder()..update(updates))._build();

  _$Gpoint_comparison_exp._(
      {this.G_eq,
      this.G_gt,
      this.G_gte,
      this.G_in,
      this.G_is_null,
      this.G_lt,
      this.G_lte,
      this.G_neq,
      this.G_nin})
      : super._();

  @override
  Gpoint_comparison_exp rebuild(
          void Function(Gpoint_comparison_expBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gpoint_comparison_expBuilder toBuilder() =>
      new Gpoint_comparison_expBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gpoint_comparison_exp &&
        G_eq == other.G_eq &&
        G_gt == other.G_gt &&
        G_gte == other.G_gte &&
        G_in == other.G_in &&
        G_is_null == other.G_is_null &&
        G_lt == other.G_lt &&
        G_lte == other.G_lte &&
        G_neq == other.G_neq &&
        G_nin == other.G_nin;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, G_eq.hashCode);
    _$hash = $jc(_$hash, G_gt.hashCode);
    _$hash = $jc(_$hash, G_gte.hashCode);
    _$hash = $jc(_$hash, G_in.hashCode);
    _$hash = $jc(_$hash, G_is_null.hashCode);
    _$hash = $jc(_$hash, G_lt.hashCode);
    _$hash = $jc(_$hash, G_lte.hashCode);
    _$hash = $jc(_$hash, G_neq.hashCode);
    _$hash = $jc(_$hash, G_nin.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gpoint_comparison_exp')
          ..add('G_eq', G_eq)
          ..add('G_gt', G_gt)
          ..add('G_gte', G_gte)
          ..add('G_in', G_in)
          ..add('G_is_null', G_is_null)
          ..add('G_lt', G_lt)
          ..add('G_lte', G_lte)
          ..add('G_neq', G_neq)
          ..add('G_nin', G_nin))
        .toString();
  }
}

class Gpoint_comparison_expBuilder
    implements Builder<Gpoint_comparison_exp, Gpoint_comparison_expBuilder> {
  _$Gpoint_comparison_exp? _$v;

  GpointBuilder? _G_eq;
  GpointBuilder get G_eq => _$this._G_eq ??= new GpointBuilder();
  set G_eq(GpointBuilder? G_eq) => _$this._G_eq = G_eq;

  GpointBuilder? _G_gt;
  GpointBuilder get G_gt => _$this._G_gt ??= new GpointBuilder();
  set G_gt(GpointBuilder? G_gt) => _$this._G_gt = G_gt;

  GpointBuilder? _G_gte;
  GpointBuilder get G_gte => _$this._G_gte ??= new GpointBuilder();
  set G_gte(GpointBuilder? G_gte) => _$this._G_gte = G_gte;

  ListBuilder<Gpoint>? _G_in;
  ListBuilder<Gpoint> get G_in => _$this._G_in ??= new ListBuilder<Gpoint>();
  set G_in(ListBuilder<Gpoint>? G_in) => _$this._G_in = G_in;

  bool? _G_is_null;
  bool? get G_is_null => _$this._G_is_null;
  set G_is_null(bool? G_is_null) => _$this._G_is_null = G_is_null;

  GpointBuilder? _G_lt;
  GpointBuilder get G_lt => _$this._G_lt ??= new GpointBuilder();
  set G_lt(GpointBuilder? G_lt) => _$this._G_lt = G_lt;

  GpointBuilder? _G_lte;
  GpointBuilder get G_lte => _$this._G_lte ??= new GpointBuilder();
  set G_lte(GpointBuilder? G_lte) => _$this._G_lte = G_lte;

  GpointBuilder? _G_neq;
  GpointBuilder get G_neq => _$this._G_neq ??= new GpointBuilder();
  set G_neq(GpointBuilder? G_neq) => _$this._G_neq = G_neq;

  ListBuilder<Gpoint>? _G_nin;
  ListBuilder<Gpoint> get G_nin => _$this._G_nin ??= new ListBuilder<Gpoint>();
  set G_nin(ListBuilder<Gpoint>? G_nin) => _$this._G_nin = G_nin;

  Gpoint_comparison_expBuilder();

  Gpoint_comparison_expBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G_eq = $v.G_eq?.toBuilder();
      _G_gt = $v.G_gt?.toBuilder();
      _G_gte = $v.G_gte?.toBuilder();
      _G_in = $v.G_in?.toBuilder();
      _G_is_null = $v.G_is_null;
      _G_lt = $v.G_lt?.toBuilder();
      _G_lte = $v.G_lte?.toBuilder();
      _G_neq = $v.G_neq?.toBuilder();
      _G_nin = $v.G_nin?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gpoint_comparison_exp other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gpoint_comparison_exp;
  }

  @override
  void update(void Function(Gpoint_comparison_expBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gpoint_comparison_exp build() => _build();

  _$Gpoint_comparison_exp _build() {
    _$Gpoint_comparison_exp _$result;
    try {
      _$result = _$v ??
          new _$Gpoint_comparison_exp._(
              G_eq: _G_eq?.build(),
              G_gt: _G_gt?.build(),
              G_gte: _G_gte?.build(),
              G_in: _G_in?.build(),
              G_is_null: G_is_null,
              G_lt: _G_lt?.build(),
              G_lte: _G_lte?.build(),
              G_neq: _G_neq?.build(),
              G_nin: _G_nin?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'G_eq';
        _G_eq?.build();
        _$failedField = 'G_gt';
        _G_gt?.build();
        _$failedField = 'G_gte';
        _G_gte?.build();
        _$failedField = 'G_in';
        _G_in?.build();

        _$failedField = 'G_lt';
        _G_lt?.build();
        _$failedField = 'G_lte';
        _G_lte?.build();
        _$failedField = 'G_neq';
        _G_neq?.build();
        _$failedField = 'G_nin';
        _G_nin?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Gpoint_comparison_exp', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Gprofiles_bool_exp extends Gprofiles_bool_exp {
  @override
  final BuiltList<Gprofiles_bool_exp>? G_and;
  @override
  final Gprofiles_bool_exp? G_not;
  @override
  final BuiltList<Gprofiles_bool_exp>? G_or;
  @override
  final GString_comparison_exp? avatar;
  @override
  final GString_comparison_exp? city;
  @override
  final GString_comparison_exp? data_cid;
  @override
  final GString_comparison_exp? description;
  @override
  final Gpoint_comparison_exp? geoloc;
  @override
  final GString_comparison_exp? index_request_cid;
  @override
  final GString_comparison_exp? pubkey;
  @override
  final Gjsonb_comparison_exp? socials;
  @override
  final Gtimestamp_comparison_exp? time;
  @override
  final GString_comparison_exp? title;

  factory _$Gprofiles_bool_exp(
          [void Function(Gprofiles_bool_expBuilder)? updates]) =>
      (new Gprofiles_bool_expBuilder()..update(updates))._build();

  _$Gprofiles_bool_exp._(
      {this.G_and,
      this.G_not,
      this.G_or,
      this.avatar,
      this.city,
      this.data_cid,
      this.description,
      this.geoloc,
      this.index_request_cid,
      this.pubkey,
      this.socials,
      this.time,
      this.title})
      : super._();

  @override
  Gprofiles_bool_exp rebuild(
          void Function(Gprofiles_bool_expBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gprofiles_bool_expBuilder toBuilder() =>
      new Gprofiles_bool_expBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gprofiles_bool_exp &&
        G_and == other.G_and &&
        G_not == other.G_not &&
        G_or == other.G_or &&
        avatar == other.avatar &&
        city == other.city &&
        data_cid == other.data_cid &&
        description == other.description &&
        geoloc == other.geoloc &&
        index_request_cid == other.index_request_cid &&
        pubkey == other.pubkey &&
        socials == other.socials &&
        time == other.time &&
        title == other.title;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, G_and.hashCode);
    _$hash = $jc(_$hash, G_not.hashCode);
    _$hash = $jc(_$hash, G_or.hashCode);
    _$hash = $jc(_$hash, avatar.hashCode);
    _$hash = $jc(_$hash, city.hashCode);
    _$hash = $jc(_$hash, data_cid.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, geoloc.hashCode);
    _$hash = $jc(_$hash, index_request_cid.hashCode);
    _$hash = $jc(_$hash, pubkey.hashCode);
    _$hash = $jc(_$hash, socials.hashCode);
    _$hash = $jc(_$hash, time.hashCode);
    _$hash = $jc(_$hash, title.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gprofiles_bool_exp')
          ..add('G_and', G_and)
          ..add('G_not', G_not)
          ..add('G_or', G_or)
          ..add('avatar', avatar)
          ..add('city', city)
          ..add('data_cid', data_cid)
          ..add('description', description)
          ..add('geoloc', geoloc)
          ..add('index_request_cid', index_request_cid)
          ..add('pubkey', pubkey)
          ..add('socials', socials)
          ..add('time', time)
          ..add('title', title))
        .toString();
  }
}

class Gprofiles_bool_expBuilder
    implements Builder<Gprofiles_bool_exp, Gprofiles_bool_expBuilder> {
  _$Gprofiles_bool_exp? _$v;

  ListBuilder<Gprofiles_bool_exp>? _G_and;
  ListBuilder<Gprofiles_bool_exp> get G_and =>
      _$this._G_and ??= new ListBuilder<Gprofiles_bool_exp>();
  set G_and(ListBuilder<Gprofiles_bool_exp>? G_and) => _$this._G_and = G_and;

  Gprofiles_bool_expBuilder? _G_not;
  Gprofiles_bool_expBuilder get G_not =>
      _$this._G_not ??= new Gprofiles_bool_expBuilder();
  set G_not(Gprofiles_bool_expBuilder? G_not) => _$this._G_not = G_not;

  ListBuilder<Gprofiles_bool_exp>? _G_or;
  ListBuilder<Gprofiles_bool_exp> get G_or =>
      _$this._G_or ??= new ListBuilder<Gprofiles_bool_exp>();
  set G_or(ListBuilder<Gprofiles_bool_exp>? G_or) => _$this._G_or = G_or;

  GString_comparison_expBuilder? _avatar;
  GString_comparison_expBuilder get avatar =>
      _$this._avatar ??= new GString_comparison_expBuilder();
  set avatar(GString_comparison_expBuilder? avatar) => _$this._avatar = avatar;

  GString_comparison_expBuilder? _city;
  GString_comparison_expBuilder get city =>
      _$this._city ??= new GString_comparison_expBuilder();
  set city(GString_comparison_expBuilder? city) => _$this._city = city;

  GString_comparison_expBuilder? _data_cid;
  GString_comparison_expBuilder get data_cid =>
      _$this._data_cid ??= new GString_comparison_expBuilder();
  set data_cid(GString_comparison_expBuilder? data_cid) =>
      _$this._data_cid = data_cid;

  GString_comparison_expBuilder? _description;
  GString_comparison_expBuilder get description =>
      _$this._description ??= new GString_comparison_expBuilder();
  set description(GString_comparison_expBuilder? description) =>
      _$this._description = description;

  Gpoint_comparison_expBuilder? _geoloc;
  Gpoint_comparison_expBuilder get geoloc =>
      _$this._geoloc ??= new Gpoint_comparison_expBuilder();
  set geoloc(Gpoint_comparison_expBuilder? geoloc) => _$this._geoloc = geoloc;

  GString_comparison_expBuilder? _index_request_cid;
  GString_comparison_expBuilder get index_request_cid =>
      _$this._index_request_cid ??= new GString_comparison_expBuilder();
  set index_request_cid(GString_comparison_expBuilder? index_request_cid) =>
      _$this._index_request_cid = index_request_cid;

  GString_comparison_expBuilder? _pubkey;
  GString_comparison_expBuilder get pubkey =>
      _$this._pubkey ??= new GString_comparison_expBuilder();
  set pubkey(GString_comparison_expBuilder? pubkey) => _$this._pubkey = pubkey;

  Gjsonb_comparison_expBuilder? _socials;
  Gjsonb_comparison_expBuilder get socials =>
      _$this._socials ??= new Gjsonb_comparison_expBuilder();
  set socials(Gjsonb_comparison_expBuilder? socials) =>
      _$this._socials = socials;

  Gtimestamp_comparison_expBuilder? _time;
  Gtimestamp_comparison_expBuilder get time =>
      _$this._time ??= new Gtimestamp_comparison_expBuilder();
  set time(Gtimestamp_comparison_expBuilder? time) => _$this._time = time;

  GString_comparison_expBuilder? _title;
  GString_comparison_expBuilder get title =>
      _$this._title ??= new GString_comparison_expBuilder();
  set title(GString_comparison_expBuilder? title) => _$this._title = title;

  Gprofiles_bool_expBuilder();

  Gprofiles_bool_expBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G_and = $v.G_and?.toBuilder();
      _G_not = $v.G_not?.toBuilder();
      _G_or = $v.G_or?.toBuilder();
      _avatar = $v.avatar?.toBuilder();
      _city = $v.city?.toBuilder();
      _data_cid = $v.data_cid?.toBuilder();
      _description = $v.description?.toBuilder();
      _geoloc = $v.geoloc?.toBuilder();
      _index_request_cid = $v.index_request_cid?.toBuilder();
      _pubkey = $v.pubkey?.toBuilder();
      _socials = $v.socials?.toBuilder();
      _time = $v.time?.toBuilder();
      _title = $v.title?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gprofiles_bool_exp other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gprofiles_bool_exp;
  }

  @override
  void update(void Function(Gprofiles_bool_expBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gprofiles_bool_exp build() => _build();

  _$Gprofiles_bool_exp _build() {
    _$Gprofiles_bool_exp _$result;
    try {
      _$result = _$v ??
          new _$Gprofiles_bool_exp._(
              G_and: _G_and?.build(),
              G_not: _G_not?.build(),
              G_or: _G_or?.build(),
              avatar: _avatar?.build(),
              city: _city?.build(),
              data_cid: _data_cid?.build(),
              description: _description?.build(),
              geoloc: _geoloc?.build(),
              index_request_cid: _index_request_cid?.build(),
              pubkey: _pubkey?.build(),
              socials: _socials?.build(),
              time: _time?.build(),
              title: _title?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'G_and';
        _G_and?.build();
        _$failedField = 'G_not';
        _G_not?.build();
        _$failedField = 'G_or';
        _G_or?.build();
        _$failedField = 'avatar';
        _avatar?.build();
        _$failedField = 'city';
        _city?.build();
        _$failedField = 'data_cid';
        _data_cid?.build();
        _$failedField = 'description';
        _description?.build();
        _$failedField = 'geoloc';
        _geoloc?.build();
        _$failedField = 'index_request_cid';
        _index_request_cid?.build();
        _$failedField = 'pubkey';
        _pubkey?.build();
        _$failedField = 'socials';
        _socials?.build();
        _$failedField = 'time';
        _time?.build();
        _$failedField = 'title';
        _title?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Gprofiles_bool_exp', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Gprofiles_order_by extends Gprofiles_order_by {
  @override
  final Gorder_by? avatar;
  @override
  final Gorder_by? city;
  @override
  final Gorder_by? data_cid;
  @override
  final Gorder_by? description;
  @override
  final Gorder_by? geoloc;
  @override
  final Gorder_by? index_request_cid;
  @override
  final Gorder_by? pubkey;
  @override
  final Gorder_by? socials;
  @override
  final Gorder_by? time;
  @override
  final Gorder_by? title;

  factory _$Gprofiles_order_by(
          [void Function(Gprofiles_order_byBuilder)? updates]) =>
      (new Gprofiles_order_byBuilder()..update(updates))._build();

  _$Gprofiles_order_by._(
      {this.avatar,
      this.city,
      this.data_cid,
      this.description,
      this.geoloc,
      this.index_request_cid,
      this.pubkey,
      this.socials,
      this.time,
      this.title})
      : super._();

  @override
  Gprofiles_order_by rebuild(
          void Function(Gprofiles_order_byBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gprofiles_order_byBuilder toBuilder() =>
      new Gprofiles_order_byBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gprofiles_order_by &&
        avatar == other.avatar &&
        city == other.city &&
        data_cid == other.data_cid &&
        description == other.description &&
        geoloc == other.geoloc &&
        index_request_cid == other.index_request_cid &&
        pubkey == other.pubkey &&
        socials == other.socials &&
        time == other.time &&
        title == other.title;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, avatar.hashCode);
    _$hash = $jc(_$hash, city.hashCode);
    _$hash = $jc(_$hash, data_cid.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, geoloc.hashCode);
    _$hash = $jc(_$hash, index_request_cid.hashCode);
    _$hash = $jc(_$hash, pubkey.hashCode);
    _$hash = $jc(_$hash, socials.hashCode);
    _$hash = $jc(_$hash, time.hashCode);
    _$hash = $jc(_$hash, title.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gprofiles_order_by')
          ..add('avatar', avatar)
          ..add('city', city)
          ..add('data_cid', data_cid)
          ..add('description', description)
          ..add('geoloc', geoloc)
          ..add('index_request_cid', index_request_cid)
          ..add('pubkey', pubkey)
          ..add('socials', socials)
          ..add('time', time)
          ..add('title', title))
        .toString();
  }
}

class Gprofiles_order_byBuilder
    implements Builder<Gprofiles_order_by, Gprofiles_order_byBuilder> {
  _$Gprofiles_order_by? _$v;

  Gorder_by? _avatar;
  Gorder_by? get avatar => _$this._avatar;
  set avatar(Gorder_by? avatar) => _$this._avatar = avatar;

  Gorder_by? _city;
  Gorder_by? get city => _$this._city;
  set city(Gorder_by? city) => _$this._city = city;

  Gorder_by? _data_cid;
  Gorder_by? get data_cid => _$this._data_cid;
  set data_cid(Gorder_by? data_cid) => _$this._data_cid = data_cid;

  Gorder_by? _description;
  Gorder_by? get description => _$this._description;
  set description(Gorder_by? description) => _$this._description = description;

  Gorder_by? _geoloc;
  Gorder_by? get geoloc => _$this._geoloc;
  set geoloc(Gorder_by? geoloc) => _$this._geoloc = geoloc;

  Gorder_by? _index_request_cid;
  Gorder_by? get index_request_cid => _$this._index_request_cid;
  set index_request_cid(Gorder_by? index_request_cid) =>
      _$this._index_request_cid = index_request_cid;

  Gorder_by? _pubkey;
  Gorder_by? get pubkey => _$this._pubkey;
  set pubkey(Gorder_by? pubkey) => _$this._pubkey = pubkey;

  Gorder_by? _socials;
  Gorder_by? get socials => _$this._socials;
  set socials(Gorder_by? socials) => _$this._socials = socials;

  Gorder_by? _time;
  Gorder_by? get time => _$this._time;
  set time(Gorder_by? time) => _$this._time = time;

  Gorder_by? _title;
  Gorder_by? get title => _$this._title;
  set title(Gorder_by? title) => _$this._title = title;

  Gprofiles_order_byBuilder();

  Gprofiles_order_byBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _avatar = $v.avatar;
      _city = $v.city;
      _data_cid = $v.data_cid;
      _description = $v.description;
      _geoloc = $v.geoloc;
      _index_request_cid = $v.index_request_cid;
      _pubkey = $v.pubkey;
      _socials = $v.socials;
      _time = $v.time;
      _title = $v.title;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gprofiles_order_by other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gprofiles_order_by;
  }

  @override
  void update(void Function(Gprofiles_order_byBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gprofiles_order_by build() => _build();

  _$Gprofiles_order_by _build() {
    final _$result = _$v ??
        new _$Gprofiles_order_by._(
            avatar: avatar,
            city: city,
            data_cid: data_cid,
            description: description,
            geoloc: geoloc,
            index_request_cid: index_request_cid,
            pubkey: pubkey,
            socials: socials,
            time: time,
            title: title);
    replace(_$result);
    return _$result;
  }
}

class _$Gprofiles_stream_cursor_input extends Gprofiles_stream_cursor_input {
  @override
  final Gprofiles_stream_cursor_value_input initial_value;
  @override
  final Gcursor_ordering? ordering;

  factory _$Gprofiles_stream_cursor_input(
          [void Function(Gprofiles_stream_cursor_inputBuilder)? updates]) =>
      (new Gprofiles_stream_cursor_inputBuilder()..update(updates))._build();

  _$Gprofiles_stream_cursor_input._(
      {required this.initial_value, this.ordering})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        initial_value, r'Gprofiles_stream_cursor_input', 'initial_value');
  }

  @override
  Gprofiles_stream_cursor_input rebuild(
          void Function(Gprofiles_stream_cursor_inputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gprofiles_stream_cursor_inputBuilder toBuilder() =>
      new Gprofiles_stream_cursor_inputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gprofiles_stream_cursor_input &&
        initial_value == other.initial_value &&
        ordering == other.ordering;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, initial_value.hashCode);
    _$hash = $jc(_$hash, ordering.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gprofiles_stream_cursor_input')
          ..add('initial_value', initial_value)
          ..add('ordering', ordering))
        .toString();
  }
}

class Gprofiles_stream_cursor_inputBuilder
    implements
        Builder<Gprofiles_stream_cursor_input,
            Gprofiles_stream_cursor_inputBuilder> {
  _$Gprofiles_stream_cursor_input? _$v;

  Gprofiles_stream_cursor_value_inputBuilder? _initial_value;
  Gprofiles_stream_cursor_value_inputBuilder get initial_value =>
      _$this._initial_value ??=
          new Gprofiles_stream_cursor_value_inputBuilder();
  set initial_value(
          Gprofiles_stream_cursor_value_inputBuilder? initial_value) =>
      _$this._initial_value = initial_value;

  Gcursor_ordering? _ordering;
  Gcursor_ordering? get ordering => _$this._ordering;
  set ordering(Gcursor_ordering? ordering) => _$this._ordering = ordering;

  Gprofiles_stream_cursor_inputBuilder();

  Gprofiles_stream_cursor_inputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _initial_value = $v.initial_value.toBuilder();
      _ordering = $v.ordering;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gprofiles_stream_cursor_input other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gprofiles_stream_cursor_input;
  }

  @override
  void update(void Function(Gprofiles_stream_cursor_inputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gprofiles_stream_cursor_input build() => _build();

  _$Gprofiles_stream_cursor_input _build() {
    _$Gprofiles_stream_cursor_input _$result;
    try {
      _$result = _$v ??
          new _$Gprofiles_stream_cursor_input._(
              initial_value: initial_value.build(), ordering: ordering);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'initial_value';
        initial_value.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Gprofiles_stream_cursor_input', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Gprofiles_stream_cursor_value_input
    extends Gprofiles_stream_cursor_value_input {
  @override
  final String? avatar;
  @override
  final String? city;
  @override
  final String? data_cid;
  @override
  final String? description;
  @override
  final Gpoint? geoloc;
  @override
  final String? index_request_cid;
  @override
  final String? pubkey;
  @override
  final _i2.JsonObject? socials;
  @override
  final Gtimestamp? time;
  @override
  final String? title;

  factory _$Gprofiles_stream_cursor_value_input(
          [void Function(Gprofiles_stream_cursor_value_inputBuilder)?
              updates]) =>
      (new Gprofiles_stream_cursor_value_inputBuilder()..update(updates))
          ._build();

  _$Gprofiles_stream_cursor_value_input._(
      {this.avatar,
      this.city,
      this.data_cid,
      this.description,
      this.geoloc,
      this.index_request_cid,
      this.pubkey,
      this.socials,
      this.time,
      this.title})
      : super._();

  @override
  Gprofiles_stream_cursor_value_input rebuild(
          void Function(Gprofiles_stream_cursor_value_inputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gprofiles_stream_cursor_value_inputBuilder toBuilder() =>
      new Gprofiles_stream_cursor_value_inputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gprofiles_stream_cursor_value_input &&
        avatar == other.avatar &&
        city == other.city &&
        data_cid == other.data_cid &&
        description == other.description &&
        geoloc == other.geoloc &&
        index_request_cid == other.index_request_cid &&
        pubkey == other.pubkey &&
        socials == other.socials &&
        time == other.time &&
        title == other.title;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, avatar.hashCode);
    _$hash = $jc(_$hash, city.hashCode);
    _$hash = $jc(_$hash, data_cid.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, geoloc.hashCode);
    _$hash = $jc(_$hash, index_request_cid.hashCode);
    _$hash = $jc(_$hash, pubkey.hashCode);
    _$hash = $jc(_$hash, socials.hashCode);
    _$hash = $jc(_$hash, time.hashCode);
    _$hash = $jc(_$hash, title.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gprofiles_stream_cursor_value_input')
          ..add('avatar', avatar)
          ..add('city', city)
          ..add('data_cid', data_cid)
          ..add('description', description)
          ..add('geoloc', geoloc)
          ..add('index_request_cid', index_request_cid)
          ..add('pubkey', pubkey)
          ..add('socials', socials)
          ..add('time', time)
          ..add('title', title))
        .toString();
  }
}

class Gprofiles_stream_cursor_value_inputBuilder
    implements
        Builder<Gprofiles_stream_cursor_value_input,
            Gprofiles_stream_cursor_value_inputBuilder> {
  _$Gprofiles_stream_cursor_value_input? _$v;

  String? _avatar;
  String? get avatar => _$this._avatar;
  set avatar(String? avatar) => _$this._avatar = avatar;

  String? _city;
  String? get city => _$this._city;
  set city(String? city) => _$this._city = city;

  String? _data_cid;
  String? get data_cid => _$this._data_cid;
  set data_cid(String? data_cid) => _$this._data_cid = data_cid;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  GpointBuilder? _geoloc;
  GpointBuilder get geoloc => _$this._geoloc ??= new GpointBuilder();
  set geoloc(GpointBuilder? geoloc) => _$this._geoloc = geoloc;

  String? _index_request_cid;
  String? get index_request_cid => _$this._index_request_cid;
  set index_request_cid(String? index_request_cid) =>
      _$this._index_request_cid = index_request_cid;

  String? _pubkey;
  String? get pubkey => _$this._pubkey;
  set pubkey(String? pubkey) => _$this._pubkey = pubkey;

  _i2.JsonObject? _socials;
  _i2.JsonObject? get socials => _$this._socials;
  set socials(_i2.JsonObject? socials) => _$this._socials = socials;

  GtimestampBuilder? _time;
  GtimestampBuilder get time => _$this._time ??= new GtimestampBuilder();
  set time(GtimestampBuilder? time) => _$this._time = time;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  Gprofiles_stream_cursor_value_inputBuilder();

  Gprofiles_stream_cursor_value_inputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _avatar = $v.avatar;
      _city = $v.city;
      _data_cid = $v.data_cid;
      _description = $v.description;
      _geoloc = $v.geoloc?.toBuilder();
      _index_request_cid = $v.index_request_cid;
      _pubkey = $v.pubkey;
      _socials = $v.socials;
      _time = $v.time?.toBuilder();
      _title = $v.title;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gprofiles_stream_cursor_value_input other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gprofiles_stream_cursor_value_input;
  }

  @override
  void update(
      void Function(Gprofiles_stream_cursor_value_inputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gprofiles_stream_cursor_value_input build() => _build();

  _$Gprofiles_stream_cursor_value_input _build() {
    _$Gprofiles_stream_cursor_value_input _$result;
    try {
      _$result = _$v ??
          new _$Gprofiles_stream_cursor_value_input._(
              avatar: avatar,
              city: city,
              data_cid: data_cid,
              description: description,
              geoloc: _geoloc?.build(),
              index_request_cid: index_request_cid,
              pubkey: pubkey,
              socials: socials,
              time: _time?.build(),
              title: title);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'geoloc';
        _geoloc?.build();

        _$failedField = 'time';
        _time?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Gprofiles_stream_cursor_value_input',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSocialInput extends GSocialInput {
  @override
  final String? type;
  @override
  final String url;

  factory _$GSocialInput([void Function(GSocialInputBuilder)? updates]) =>
      (new GSocialInputBuilder()..update(updates))._build();

  _$GSocialInput._({this.type, required this.url}) : super._() {
    BuiltValueNullFieldError.checkNotNull(url, r'GSocialInput', 'url');
  }

  @override
  GSocialInput rebuild(void Function(GSocialInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSocialInputBuilder toBuilder() => new GSocialInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSocialInput && type == other.type && url == other.url;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, url.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GSocialInput')
          ..add('type', type)
          ..add('url', url))
        .toString();
  }
}

class GSocialInputBuilder
    implements Builder<GSocialInput, GSocialInputBuilder> {
  _$GSocialInput? _$v;

  String? _type;
  String? get type => _$this._type;
  set type(String? type) => _$this._type = type;

  String? _url;
  String? get url => _$this._url;
  set url(String? url) => _$this._url = url;

  GSocialInputBuilder();

  GSocialInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _url = $v.url;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSocialInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSocialInput;
  }

  @override
  void update(void Function(GSocialInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GSocialInput build() => _build();

  _$GSocialInput _build() {
    final _$result = _$v ??
        new _$GSocialInput._(
            type: type,
            url: BuiltValueNullFieldError.checkNotNull(
                url, r'GSocialInput', 'url'));
    replace(_$result);
    return _$result;
  }
}

class _$GString_comparison_exp extends GString_comparison_exp {
  @override
  final String? G_eq;
  @override
  final String? G_gt;
  @override
  final String? G_gte;
  @override
  final String? G_ilike;
  @override
  final BuiltList<String>? G_in;
  @override
  final String? G_iregex;
  @override
  final bool? G_is_null;
  @override
  final String? G_like;
  @override
  final String? G_lt;
  @override
  final String? G_lte;
  @override
  final String? G_neq;
  @override
  final String? G_nilike;
  @override
  final BuiltList<String>? G_nin;
  @override
  final String? G_niregex;
  @override
  final String? G_nlike;
  @override
  final String? G_nregex;
  @override
  final String? G_nsimilar;
  @override
  final String? G_regex;
  @override
  final String? G_similar;

  factory _$GString_comparison_exp(
          [void Function(GString_comparison_expBuilder)? updates]) =>
      (new GString_comparison_expBuilder()..update(updates))._build();

  _$GString_comparison_exp._(
      {this.G_eq,
      this.G_gt,
      this.G_gte,
      this.G_ilike,
      this.G_in,
      this.G_iregex,
      this.G_is_null,
      this.G_like,
      this.G_lt,
      this.G_lte,
      this.G_neq,
      this.G_nilike,
      this.G_nin,
      this.G_niregex,
      this.G_nlike,
      this.G_nregex,
      this.G_nsimilar,
      this.G_regex,
      this.G_similar})
      : super._();

  @override
  GString_comparison_exp rebuild(
          void Function(GString_comparison_expBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GString_comparison_expBuilder toBuilder() =>
      new GString_comparison_expBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GString_comparison_exp &&
        G_eq == other.G_eq &&
        G_gt == other.G_gt &&
        G_gte == other.G_gte &&
        G_ilike == other.G_ilike &&
        G_in == other.G_in &&
        G_iregex == other.G_iregex &&
        G_is_null == other.G_is_null &&
        G_like == other.G_like &&
        G_lt == other.G_lt &&
        G_lte == other.G_lte &&
        G_neq == other.G_neq &&
        G_nilike == other.G_nilike &&
        G_nin == other.G_nin &&
        G_niregex == other.G_niregex &&
        G_nlike == other.G_nlike &&
        G_nregex == other.G_nregex &&
        G_nsimilar == other.G_nsimilar &&
        G_regex == other.G_regex &&
        G_similar == other.G_similar;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, G_eq.hashCode);
    _$hash = $jc(_$hash, G_gt.hashCode);
    _$hash = $jc(_$hash, G_gte.hashCode);
    _$hash = $jc(_$hash, G_ilike.hashCode);
    _$hash = $jc(_$hash, G_in.hashCode);
    _$hash = $jc(_$hash, G_iregex.hashCode);
    _$hash = $jc(_$hash, G_is_null.hashCode);
    _$hash = $jc(_$hash, G_like.hashCode);
    _$hash = $jc(_$hash, G_lt.hashCode);
    _$hash = $jc(_$hash, G_lte.hashCode);
    _$hash = $jc(_$hash, G_neq.hashCode);
    _$hash = $jc(_$hash, G_nilike.hashCode);
    _$hash = $jc(_$hash, G_nin.hashCode);
    _$hash = $jc(_$hash, G_niregex.hashCode);
    _$hash = $jc(_$hash, G_nlike.hashCode);
    _$hash = $jc(_$hash, G_nregex.hashCode);
    _$hash = $jc(_$hash, G_nsimilar.hashCode);
    _$hash = $jc(_$hash, G_regex.hashCode);
    _$hash = $jc(_$hash, G_similar.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GString_comparison_exp')
          ..add('G_eq', G_eq)
          ..add('G_gt', G_gt)
          ..add('G_gte', G_gte)
          ..add('G_ilike', G_ilike)
          ..add('G_in', G_in)
          ..add('G_iregex', G_iregex)
          ..add('G_is_null', G_is_null)
          ..add('G_like', G_like)
          ..add('G_lt', G_lt)
          ..add('G_lte', G_lte)
          ..add('G_neq', G_neq)
          ..add('G_nilike', G_nilike)
          ..add('G_nin', G_nin)
          ..add('G_niregex', G_niregex)
          ..add('G_nlike', G_nlike)
          ..add('G_nregex', G_nregex)
          ..add('G_nsimilar', G_nsimilar)
          ..add('G_regex', G_regex)
          ..add('G_similar', G_similar))
        .toString();
  }
}

class GString_comparison_expBuilder
    implements Builder<GString_comparison_exp, GString_comparison_expBuilder> {
  _$GString_comparison_exp? _$v;

  String? _G_eq;
  String? get G_eq => _$this._G_eq;
  set G_eq(String? G_eq) => _$this._G_eq = G_eq;

  String? _G_gt;
  String? get G_gt => _$this._G_gt;
  set G_gt(String? G_gt) => _$this._G_gt = G_gt;

  String? _G_gte;
  String? get G_gte => _$this._G_gte;
  set G_gte(String? G_gte) => _$this._G_gte = G_gte;

  String? _G_ilike;
  String? get G_ilike => _$this._G_ilike;
  set G_ilike(String? G_ilike) => _$this._G_ilike = G_ilike;

  ListBuilder<String>? _G_in;
  ListBuilder<String> get G_in => _$this._G_in ??= new ListBuilder<String>();
  set G_in(ListBuilder<String>? G_in) => _$this._G_in = G_in;

  String? _G_iregex;
  String? get G_iregex => _$this._G_iregex;
  set G_iregex(String? G_iregex) => _$this._G_iregex = G_iregex;

  bool? _G_is_null;
  bool? get G_is_null => _$this._G_is_null;
  set G_is_null(bool? G_is_null) => _$this._G_is_null = G_is_null;

  String? _G_like;
  String? get G_like => _$this._G_like;
  set G_like(String? G_like) => _$this._G_like = G_like;

  String? _G_lt;
  String? get G_lt => _$this._G_lt;
  set G_lt(String? G_lt) => _$this._G_lt = G_lt;

  String? _G_lte;
  String? get G_lte => _$this._G_lte;
  set G_lte(String? G_lte) => _$this._G_lte = G_lte;

  String? _G_neq;
  String? get G_neq => _$this._G_neq;
  set G_neq(String? G_neq) => _$this._G_neq = G_neq;

  String? _G_nilike;
  String? get G_nilike => _$this._G_nilike;
  set G_nilike(String? G_nilike) => _$this._G_nilike = G_nilike;

  ListBuilder<String>? _G_nin;
  ListBuilder<String> get G_nin => _$this._G_nin ??= new ListBuilder<String>();
  set G_nin(ListBuilder<String>? G_nin) => _$this._G_nin = G_nin;

  String? _G_niregex;
  String? get G_niregex => _$this._G_niregex;
  set G_niregex(String? G_niregex) => _$this._G_niregex = G_niregex;

  String? _G_nlike;
  String? get G_nlike => _$this._G_nlike;
  set G_nlike(String? G_nlike) => _$this._G_nlike = G_nlike;

  String? _G_nregex;
  String? get G_nregex => _$this._G_nregex;
  set G_nregex(String? G_nregex) => _$this._G_nregex = G_nregex;

  String? _G_nsimilar;
  String? get G_nsimilar => _$this._G_nsimilar;
  set G_nsimilar(String? G_nsimilar) => _$this._G_nsimilar = G_nsimilar;

  String? _G_regex;
  String? get G_regex => _$this._G_regex;
  set G_regex(String? G_regex) => _$this._G_regex = G_regex;

  String? _G_similar;
  String? get G_similar => _$this._G_similar;
  set G_similar(String? G_similar) => _$this._G_similar = G_similar;

  GString_comparison_expBuilder();

  GString_comparison_expBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G_eq = $v.G_eq;
      _G_gt = $v.G_gt;
      _G_gte = $v.G_gte;
      _G_ilike = $v.G_ilike;
      _G_in = $v.G_in?.toBuilder();
      _G_iregex = $v.G_iregex;
      _G_is_null = $v.G_is_null;
      _G_like = $v.G_like;
      _G_lt = $v.G_lt;
      _G_lte = $v.G_lte;
      _G_neq = $v.G_neq;
      _G_nilike = $v.G_nilike;
      _G_nin = $v.G_nin?.toBuilder();
      _G_niregex = $v.G_niregex;
      _G_nlike = $v.G_nlike;
      _G_nregex = $v.G_nregex;
      _G_nsimilar = $v.G_nsimilar;
      _G_regex = $v.G_regex;
      _G_similar = $v.G_similar;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GString_comparison_exp other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GString_comparison_exp;
  }

  @override
  void update(void Function(GString_comparison_expBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GString_comparison_exp build() => _build();

  _$GString_comparison_exp _build() {
    _$GString_comparison_exp _$result;
    try {
      _$result = _$v ??
          new _$GString_comparison_exp._(
              G_eq: G_eq,
              G_gt: G_gt,
              G_gte: G_gte,
              G_ilike: G_ilike,
              G_in: _G_in?.build(),
              G_iregex: G_iregex,
              G_is_null: G_is_null,
              G_like: G_like,
              G_lt: G_lt,
              G_lte: G_lte,
              G_neq: G_neq,
              G_nilike: G_nilike,
              G_nin: _G_nin?.build(),
              G_niregex: G_niregex,
              G_nlike: G_nlike,
              G_nregex: G_nregex,
              G_nsimilar: G_nsimilar,
              G_regex: G_regex,
              G_similar: G_similar);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'G_in';
        _G_in?.build();

        _$failedField = 'G_nin';
        _G_nin?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GString_comparison_exp', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Gtimestamp extends Gtimestamp {
  @override
  final String value;

  factory _$Gtimestamp([void Function(GtimestampBuilder)? updates]) =>
      (new GtimestampBuilder()..update(updates))._build();

  _$Gtimestamp._({required this.value}) : super._() {
    BuiltValueNullFieldError.checkNotNull(value, r'Gtimestamp', 'value');
  }

  @override
  Gtimestamp rebuild(void Function(GtimestampBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GtimestampBuilder toBuilder() => new GtimestampBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gtimestamp && value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gtimestamp')..add('value', value))
        .toString();
  }
}

class GtimestampBuilder implements Builder<Gtimestamp, GtimestampBuilder> {
  _$Gtimestamp? _$v;

  String? _value;
  String? get value => _$this._value;
  set value(String? value) => _$this._value = value;

  GtimestampBuilder();

  GtimestampBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gtimestamp other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gtimestamp;
  }

  @override
  void update(void Function(GtimestampBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gtimestamp build() => _build();

  _$Gtimestamp _build() {
    final _$result = _$v ??
        new _$Gtimestamp._(
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'Gtimestamp', 'value'));
    replace(_$result);
    return _$result;
  }
}

class _$Gtimestamp_comparison_exp extends Gtimestamp_comparison_exp {
  @override
  final Gtimestamp? G_eq;
  @override
  final Gtimestamp? G_gt;
  @override
  final Gtimestamp? G_gte;
  @override
  final BuiltList<Gtimestamp>? G_in;
  @override
  final bool? G_is_null;
  @override
  final Gtimestamp? G_lt;
  @override
  final Gtimestamp? G_lte;
  @override
  final Gtimestamp? G_neq;
  @override
  final BuiltList<Gtimestamp>? G_nin;

  factory _$Gtimestamp_comparison_exp(
          [void Function(Gtimestamp_comparison_expBuilder)? updates]) =>
      (new Gtimestamp_comparison_expBuilder()..update(updates))._build();

  _$Gtimestamp_comparison_exp._(
      {this.G_eq,
      this.G_gt,
      this.G_gte,
      this.G_in,
      this.G_is_null,
      this.G_lt,
      this.G_lte,
      this.G_neq,
      this.G_nin})
      : super._();

  @override
  Gtimestamp_comparison_exp rebuild(
          void Function(Gtimestamp_comparison_expBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Gtimestamp_comparison_expBuilder toBuilder() =>
      new Gtimestamp_comparison_expBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gtimestamp_comparison_exp &&
        G_eq == other.G_eq &&
        G_gt == other.G_gt &&
        G_gte == other.G_gte &&
        G_in == other.G_in &&
        G_is_null == other.G_is_null &&
        G_lt == other.G_lt &&
        G_lte == other.G_lte &&
        G_neq == other.G_neq &&
        G_nin == other.G_nin;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, G_eq.hashCode);
    _$hash = $jc(_$hash, G_gt.hashCode);
    _$hash = $jc(_$hash, G_gte.hashCode);
    _$hash = $jc(_$hash, G_in.hashCode);
    _$hash = $jc(_$hash, G_is_null.hashCode);
    _$hash = $jc(_$hash, G_lt.hashCode);
    _$hash = $jc(_$hash, G_lte.hashCode);
    _$hash = $jc(_$hash, G_neq.hashCode);
    _$hash = $jc(_$hash, G_nin.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Gtimestamp_comparison_exp')
          ..add('G_eq', G_eq)
          ..add('G_gt', G_gt)
          ..add('G_gte', G_gte)
          ..add('G_in', G_in)
          ..add('G_is_null', G_is_null)
          ..add('G_lt', G_lt)
          ..add('G_lte', G_lte)
          ..add('G_neq', G_neq)
          ..add('G_nin', G_nin))
        .toString();
  }
}

class Gtimestamp_comparison_expBuilder
    implements
        Builder<Gtimestamp_comparison_exp, Gtimestamp_comparison_expBuilder> {
  _$Gtimestamp_comparison_exp? _$v;

  GtimestampBuilder? _G_eq;
  GtimestampBuilder get G_eq => _$this._G_eq ??= new GtimestampBuilder();
  set G_eq(GtimestampBuilder? G_eq) => _$this._G_eq = G_eq;

  GtimestampBuilder? _G_gt;
  GtimestampBuilder get G_gt => _$this._G_gt ??= new GtimestampBuilder();
  set G_gt(GtimestampBuilder? G_gt) => _$this._G_gt = G_gt;

  GtimestampBuilder? _G_gte;
  GtimestampBuilder get G_gte => _$this._G_gte ??= new GtimestampBuilder();
  set G_gte(GtimestampBuilder? G_gte) => _$this._G_gte = G_gte;

  ListBuilder<Gtimestamp>? _G_in;
  ListBuilder<Gtimestamp> get G_in =>
      _$this._G_in ??= new ListBuilder<Gtimestamp>();
  set G_in(ListBuilder<Gtimestamp>? G_in) => _$this._G_in = G_in;

  bool? _G_is_null;
  bool? get G_is_null => _$this._G_is_null;
  set G_is_null(bool? G_is_null) => _$this._G_is_null = G_is_null;

  GtimestampBuilder? _G_lt;
  GtimestampBuilder get G_lt => _$this._G_lt ??= new GtimestampBuilder();
  set G_lt(GtimestampBuilder? G_lt) => _$this._G_lt = G_lt;

  GtimestampBuilder? _G_lte;
  GtimestampBuilder get G_lte => _$this._G_lte ??= new GtimestampBuilder();
  set G_lte(GtimestampBuilder? G_lte) => _$this._G_lte = G_lte;

  GtimestampBuilder? _G_neq;
  GtimestampBuilder get G_neq => _$this._G_neq ??= new GtimestampBuilder();
  set G_neq(GtimestampBuilder? G_neq) => _$this._G_neq = G_neq;

  ListBuilder<Gtimestamp>? _G_nin;
  ListBuilder<Gtimestamp> get G_nin =>
      _$this._G_nin ??= new ListBuilder<Gtimestamp>();
  set G_nin(ListBuilder<Gtimestamp>? G_nin) => _$this._G_nin = G_nin;

  Gtimestamp_comparison_expBuilder();

  Gtimestamp_comparison_expBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G_eq = $v.G_eq?.toBuilder();
      _G_gt = $v.G_gt?.toBuilder();
      _G_gte = $v.G_gte?.toBuilder();
      _G_in = $v.G_in?.toBuilder();
      _G_is_null = $v.G_is_null;
      _G_lt = $v.G_lt?.toBuilder();
      _G_lte = $v.G_lte?.toBuilder();
      _G_neq = $v.G_neq?.toBuilder();
      _G_nin = $v.G_nin?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gtimestamp_comparison_exp other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gtimestamp_comparison_exp;
  }

  @override
  void update(void Function(Gtimestamp_comparison_expBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Gtimestamp_comparison_exp build() => _build();

  _$Gtimestamp_comparison_exp _build() {
    _$Gtimestamp_comparison_exp _$result;
    try {
      _$result = _$v ??
          new _$Gtimestamp_comparison_exp._(
              G_eq: _G_eq?.build(),
              G_gt: _G_gt?.build(),
              G_gte: _G_gte?.build(),
              G_in: _G_in?.build(),
              G_is_null: G_is_null,
              G_lt: _G_lt?.build(),
              G_lte: _G_lte?.build(),
              G_neq: _G_neq?.build(),
              G_nin: _G_nin?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'G_eq';
        _G_eq?.build();
        _$failedField = 'G_gt';
        _G_gt?.build();
        _$failedField = 'G_gte';
        _G_gte?.build();
        _$failedField = 'G_in';
        _G_in?.build();

        _$failedField = 'G_lt';
        _G_lt?.build();
        _$failedField = 'G_lte';
        _G_lte?.build();
        _$failedField = 'G_neq';
        _G_neq?.build();
        _$failedField = 'G_nin';
        _G_nin?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Gtimestamp_comparison_exp', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
